/**
 * Copyright (c) 2013 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "sdk_common.h"
#include "app_error.h"
#if NRF_MODULE_ENABLED(BLE_LBS)
#include "ble_lbs.h"
#include "ble_srv_common.h"
#include <stdlib.h>
#define NRF_LOG_MODULE_NAME ble_lbs
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


/**@brief Function for handling the Write event.
 *
 * @param[in] p_lbs      LED Button Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
uint8_t gft4_rx_buffer[64];
static void on_rx_write(ble_lbs_t * p_lbs, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    memcpy(gft4_rx_buffer, p_evt_write->data, p_evt_write->len);
    if (   (p_evt_write->handle == p_lbs->gft4_rx_char_handles.value_handle)
        && (p_evt_write->len >= 1)
        && (p_lbs->gft4_write_handler != NULL))
    {
        p_lbs->gft4_write_handler(p_ble_evt->evt.gap_evt.conn_handle, p_lbs, gft4_rx_buffer, p_evt_write->len);
    }
}

bool tx_success = true;
void ble_lbs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_lbs_t * p_lbs = (ble_lbs_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
            on_rx_write(p_lbs, p_ble_evt);
            break;
        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
					  NRF_LOG_INFO("BLE_GATTS_EVT_HVN_TX_COMPLETE ==> 0x%x", BLE_GATTS_EVT_HVN_TX_COMPLETE);
				    tx_success = true;
					  break;
        default:
            // No implementation needed.
            break;
    }
}

#define MAX_PACKET_LENGTH 67
uint32_t ble_lbs_init(ble_lbs_t * p_lbs, const ble_lbs_init_t * p_lbs_init)
{
    uint32_t              err_code;
    ble_uuid_t            ble_uuid;
    ble_add_char_params_t add_char_params;   
	
    // Initialize service structure.
    p_lbs->gft4_write_handler = p_lbs_init->gft4_write_handler;                  // Write LED state

    // Add service.
    ble_uuid128_t base_uuid = {LBS_UUID_BASE};
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_lbs->uuid_type);
		NRF_LOG_DEBUG("===>p_lbs->uuid_type = %d", p_lbs->uuid_type);               // Should be equal to "2" ---> Vendor UUID types by Jason
    VERIFY_SUCCESS(err_code);		

    ble_uuid.type = p_lbs->uuid_type;
    ble_uuid.uuid = LBS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_lbs->service_handle);		
    VERIFY_SUCCESS(err_code);		
#if 0
typedef struct
{
    uint16_t                    uuid;                     /**< Characteristic UUID (16 bits UUIDs).*/
    uint8_t                     uuid_type;                /**< Base UUID. If 0, the Bluetooth SIG UUID will be used. Otherwise, this should be a value returned by @ref sd_ble_uuid_vs_add when adding the base UUID.*/
    uint16_t                    max_len;                  /**< Maximum length of the characteristic value.*/
    uint16_t                    init_len;                 /**< Initial length of the characteristic value.*/
    uint8_t *                   p_init_value;             /**< Initial encoded value of the characteristic.*/
    bool                        is_var_len;               /**< Indicates if the characteristic value has variable length.*/
    ble_gatt_char_props_t       char_props;               /**< Characteristic properties.*/
    ble_gatt_char_ext_props_t   char_ext_props;           /**< Characteristic extended properties.*/
    bool                        is_defered_read;          /**< Indicate if deferred read operations are supported.*/
    bool                        is_defered_write;         /**< Indicate if deferred write operations are supported.*/
    security_req_t              read_access;              /**< Security requirement for reading the characteristic value.*/
    security_req_t              write_access;             /**< Security requirement for writing the characteristic value.*/
    security_req_t              cccd_write_access;        /**< Security requirement for writing the characteristic's CCCD.*/
    bool                        is_value_user;            /**< Indicate if the content of the characteristic is to be stored in the application (user) or in the stack.*/
    ble_add_char_user_desc_t    *p_user_descr;            /**< Pointer to user descriptor if needed*/
    ble_gatts_char_pf_t         *p_presentation_format;   /**< Pointer to characteristic format if needed*/
} ble_add_char_params_t;
#endif
    // Add Button characteristic.
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid              = LBS_UUID_BUTTON_CHAR;
    add_char_params.uuid_type         = p_lbs->uuid_type;
    add_char_params.init_len          = sizeof(uint8_t);
    add_char_params.max_len           = MAX_PACKET_LENGTH;//32;//sizeof(uint8_t);
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.notify = 1;
    add_char_params.is_var_len        = true;                    //   jason
		
    add_char_params.read_access       = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN;

    err_code = characteristic_add(p_lbs->service_handle,
                                  &add_char_params,
                                  &p_lbs->gft4_tx_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add LED characteristic.
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid             = LBS_UUID_LED_CHAR;
    add_char_params.uuid_type        = p_lbs->uuid_type;
    add_char_params.init_len         = sizeof(uint8_t);
    add_char_params.max_len          = MAX_PACKET_LENGTH;//sizeof(uint8_t);
    add_char_params.char_props.read  = 1;
    add_char_params.char_props.write = 1;
		add_char_params.is_var_len       = true;                    //   jason

    add_char_params.read_access  = SEC_OPEN;
    add_char_params.write_access = SEC_OPEN;

    return characteristic_add(p_lbs->service_handle, &add_char_params, &p_lbs->gft4_rx_char_handles);
}

// button state notification to host (smart phone)     Jason
uint16_t len;
uint8_t tx_data[MAX_PACKET_LENGTH];
extern ble_gap_addr_t device_addr;
//extern void bsp_board_led_on(uint32_t led_idx);
//extern void bsp_board_led_off(uint32_t led_idx);
//#define BLUE_LED                                  2                         
uint32_t ble_lbs_on_button_change(uint16_t conn_handle, ble_lbs_t * p_lbs, uint8_t data, uint16_t value16)
{	  
	  uint8_t k;
	  //static uint8_t button_status = 0;
		
	  tx_data[0] = data;//button_state;//button_status;   //button_state;
	  tx_data[1] = 0x55;//device_addr.addr[0];
		tx_data[2] = (value16 >> 8) & 0xFF;
		tx_data[3] = (value16 >> 0) & 0xFF;
		tx_data[4] = device_addr.addr[0];
		tx_data[5] = device_addr.addr[1];
		tx_data[6] = device_addr.addr[2];
		tx_data[7] = device_addr.addr[3];
		tx_data[8] = device_addr.addr[4];
	  tx_data[9] = device_addr.addr[5];
	
    static ble_gatts_hvx_params_t params;
    len = rand()%MAX_PACKET_LENGTH;//sizeof(button_state);
	  if(len <= 10) 
		{
			len = 10;
		  tx_data[len]++;
		}
		else if(len > 10)
		{
	    for(k = 10; k < len; k++)
		    tx_data[k]++;
		}
    
    memset(&params, 0, sizeof(params));
    params.type   = BLE_GATT_HVX_NOTIFICATION;
    params.handle = p_lbs->gft4_tx_char_handles.value_handle;
    params.p_data = &tx_data[0];
    params.p_len  = &len;

    return sd_ble_gatts_hvx(conn_handle, &params);
}

ret_code_t ble_packet_send(uint16_t conn_handle, ble_lbs_t * p_lbs, uint8_t * data, uint16_t length)
{
	  ret_code_t err_code;	
	  uint8_t k, *p_data = data;
	  uint16_t len = length;
	  static uint8_t led_toggle;
	
	  led_toggle = !led_toggle;
	
	  if(len > MAX_PACKET_LENGTH)
			len = MAX_PACKET_LENGTH;

	  tx_data[0] = LBS_UUID_ID;
	  tx_data[1] = led_toggle;		
	  for( k = 0; k < len; k++)
	  {
       tx_data[k + 2] = *p_data;
			 p_data++;
		}
	  
		len = len + 2;
    static ble_gatts_hvx_params_t params;    
    memset(&params, 0, sizeof(params));
    params.type   = BLE_GATT_HVX_NOTIFICATION;
    params.handle = p_lbs->gft4_tx_char_handles.value_handle;
    params.p_data = &tx_data[0];
    params.p_len  = &len;

    err_code = sd_ble_gatts_hvx(conn_handle, &params);	
		return err_code;
}
#endif // NRF_MODULE_ENABLED(BLE_LBS)
