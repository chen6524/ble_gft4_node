/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/

/********************************************************************************
*  
*
* History:
*
* 04/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#include "LSM6DS3.h"
#include <string.h>
#include "mGyro.h"
//#include "mImpact.h"
//#include "adc.h"
//#include "mHid.h"
//#include "kbi.h"
//#include "bcd_common.h"                       // Added by Jason Chen for time stamp fix, 2015.05.07

GYRO_INFO gyroInfo[2];                          //#define ACC_MAX_BD_NUM  0x2

GYRO_STATE gyroState=GYRO_STATE_SOFT_RESET;

volatile uint8_t lgIsrState =0;
//volatile static ACC_DATA accData; 

uint8_t Statup_flag = 3;                          // Added by Jason Chen for first time startup, 2017.03.30

void lgISR (void)
{
   //??DISABLE_LOWG_INT;
   if(Statup_flag) 
   {
      Statup_flag--;
      //??imSleepTimerTime = 75;                   // Added by Jason Chen, 2017.03.30
   } 
   else   
      ;//??imSleepTimerTime = IM_SLEEP_TIMER;       // Added by Jason Chen, 2014.11.26
   //??pwrOffCnt =0;
   return;
   #if 0
   lgIsrState++;
   //adc sample now
    if (!KBI_USB_ON) accHitStat = ACC_HIT_STAT_INIT;
   imSleepTimerTime = IM_SLEEP_TIMER;
   DISABLE_LOWG_INT;
   return;
   #endif
}

void gyroInit(void)
{
	 //volatile byte a;
//	 byte i;
    SPI_MEMS_Acc_Config( 0);
    SPI_MEMS_Gyro_Config();
  //a = acc_SPIReadReg(0x3a ); 
	//a = acc_SPIReadReg(0x3b ); 	
	//gyroGet();    
  //set stream mode
  //gyro_FifoStream();
//#endif 
}

/* 
	the sample rate for the gyro is 760Hz
*/
#define GYRO_SAMPLE_INTERVAL (1/760)
uint8_t gyroGet(void)
{
#ifdef ENABLE_6DS3_G

   byte i;
//   volatile byte a;

   GYRO_INFO *pGyro = pAccNowW->pGyroInfo;
   //there are 32  sets of data 
   for(i = 0; i<32; i++)
   {   		  		
   		 gyro_FIFO_SPIGetRawData((uint8_t*)(&(pGyro->fifoData[i])));
		 
   		
		//a = acc_SPIReadReg(0x3a ); 
   }
#endif
   return 1;
}


#define     gyroGetSum(x,y,z) (x*x+y*y+z*z)

uint8_t gyroCalc(void)
{ 
#if 0	
  uint8_t i;
  
  //int16_t t1 =0, t2=0, t3 =0;
  //byte which = 0;
  int32_t t = 0, r = 0;
  //int16_t s1 =0, s2=0, s3 =0;
  GYRO_INFO *pGyro = pAccNowR->pGyroInfo;
   //calculate the max acc
  for(i = 1; i<32; i++)
  {  	  

	#if 0
		t1 = (pGyro->fifoData[i].x- pGyro->fifoData[i-1].x);
		s1 = t1;
		if (t1<0){t1 = -t1; }
		
		t2 = (pGyro->fifoData[i].y- pGyro->fifoData[i-1].y);
		s2 = t2;
		if (t2<0) {t2 = -t2;}
		
		t3 = (pGyro->fifoData[i].z- pGyro->fifoData[i-1].z);
		s3 = t3;
		if (t3<0 ) { t3 = -t3;}
		
		t =  t1+t2+t3;
	#endif
        
    #if ENABLE_ACC_DATA_OUTPUT                                      // Added by Jason Chen for acc test,2014.05.07
      t =  gyroGetSum((int32_t)(pGyro->fifoData[i].x >> 4) , (int32_t)(pGyro->fifoData[i].y >> 4) , (int32_t)(pGyro->fifoData[i].z>>4));	    
    #else
      t =  gyroGetSum((int32_t)pGyro->fifoData[i].x , (int32_t)pGyro->fifoData[i].y , (int32_t)pGyro->fifoData[i].z);	    
    #endif
    
		if( t > r )
		{	
			 r =t;
			 pGyro->maxAcc.x = pGyro->fifoData[i].x;///GYRO_SAMPLE_INTERVAL; let do the calculation in PC
       pGyro->maxAcc.y = pGyro->fifoData[i].y;///GYRO_SAMPLE_INTERVAL;
       pGyro->maxAcc.z = pGyro->fifoData[i].z;///GYRO_SAMPLE_INTERVAL;
		}			
		//pAccNowR->pEntry->dataG = pGyro->
   }
  
        //copy data 
		(void)memcpy(pAccNowR->pEntry->dataG,pGyro->fifoData,sizeof(GYRO_DATA)*GYRO_FIFO_SIZE);
		pAccNowR->pEntry->gyroActive.gyro.x = pGyro->maxAcc.x;
		pAccNowR->pEntry->gyroActive.gyro.y = pGyro->maxAcc.y;
	    pAccNowR->pEntry->gyroActive.gyro.z = pGyro->maxAcc.z;
#endif	    
   return 1;
   
}

//static LOWG_DATA lgAccData[32];
#define BUFFER_SIZE  30
//static LOWG_DATA lgAccVData[BUFFER_SIZE];                                 // Added by Jason Chen for preventing wrong recording , 2014.05.06 
LOWG_DATA lgAccV;
LOWG_DATA lgGyroV;
uint8_t lgAccGet(void) {
  
   acc_FIFO_SPIGetRawData((uint8_t *)(&lgAccV));
   
   return 1;
}
uint8_t lgGyroGet(void)
{
	

	   return 1;
}

void lgAccGetData(void)                                  // Added by Jason Chen for preventing wrong recording , 2014.05.06 
{
   uint8_t i;
   //there are 32  sets of data 
   for(i = 0; i<32; i++)
   {
   		//gyro_FIFO_SPIGetRawData((uint8_t*)(&(pGyro->fifoData[i])));
   		//acc_FIFO_SPIGetRawData((byte *)(&lgAccVData[i]));		
   }
}

#define THRESHOLD_VALUE_TT                     62500L                 // 250 * 250
uint8_t getMaxThresh(void)                                 // Added by Jason Chen for preventing wrong recording , 2014.05.06 
{
   uint8_t i;

   int32_t t = 0, r = 0;
     
   //lgAccGetData();   
     
   for(i = 0; i< BUFFER_SIZE; i++)
   {  	          
      acc_FIFO_SPIGetRawData((uint8_t *)(&lgAccV));		
      #if ENABLE_ACC_DATA_OUTPUT                                      // Added by Jason Chen for acc test,2014.05.07
         pAccNowW->pGyroInfo->fifoData[i].x = lgAccV.x;
         pAccNowW->pGyroInfo->fifoData[i].y = lgAccV.y;
         pAccNowW->pGyroInfo->fifoData[i].z = lgAccV.z;
      #endif
      t =  gyroGetSum((int32_t)(lgAccV.x>>4) , (int32_t)(lgAccV.y>>4) , (int32_t)(lgAccV.z>>4));	    
      if( t > r )
    	  r = t;
   }
// t = (thresR * 1000)/12;           // 12mg/digit       +/- 16g configuration
 //t = 250    ;                      // (250 * 12)/1000 = 3.0 g
   t = THRESHOLD_VALUE_TT;           // (250 * 12)/1000 = 3.0 g
   if( r > t)    
     return 1;
   else            
     return 0;
}

static uint8_t lgAccCalc(void)
{
    //lgAccV.x = lgAccData[15].x;
    //lgAccV.y = lgAccData[15].y;
    //lgAccV.z = lgAccData[15].z;
    return 1;
}

static uint8_t lgState = LG_STATE_INIT;

void lowGTask(void)
{
    static uint8_t cnt = 0;
    switch(lgState)
      {
          case LG_STATE_INIT:     
             
              //SPI_MEMS_Acc_Config(1);
              lgAccV.x = 0;
              lgAccV.y = 0;
              lgAccV.z = 0;
              
              lgState = LG_STATE_CAL_START;
              break;
          case LG_STATE_CAL_START:
              
              lgAccGet();
              if(cnt++ == 2)
              {
                lgState = LG_STATE_CAL_CAC;
                cnt = 0;
              }
            break;
          case LG_STATE_CAL_CAC:
              lgAccCalc();
              lgState = LG_STATE_CAL_END;
            break;
          case LG_STATE_CAL_END:
              lgState = LG_STATE_INIT;
              //??startCal =0;
          break;
    }

}



/**
 * @}
 */
