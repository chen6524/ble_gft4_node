/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
 
#ifndef _MGYRO_H_
#define _MGYRO_H_

#include <stdint.h>

typedef enum 
{
	  GYRO_STATE_NONE,
    GYRO_STATE_INIT,
    GYRO_STATE_WAIT, 
    GYRO_STATE_IDLE,
    GYRO_STATE_READ, 
    GYRO_STATE_CALC,
    GYRO_STATE_SAVE,
    GYRO_STATE_SOFT_RESET
}GYRO_STATE;


typedef enum 
{
    LG_STATE_INIT,
    LG_STATE_CAL_START,
    LG_STATE_CAL_CAC,
    LG_STATE_CAL_END,
    LG_STATE_IDLE
    
    
}LOWG_STATE;

typedef struct 
{
	int16_t x;
	int16_t y;
	int16_t z;

} GYRO_DATA;

typedef struct 
{
	int16_t x;
	int16_t y;
	int16_t z;

} LOWG_DATA;

typedef struct 
{
	GYRO_DATA maxAcc;
	GYRO_DATA fifoData[32];

} GYRO_INFO;

extern GYRO_INFO gyroInfo[];
extern volatile uint8_t lgIsrState;
extern LOWG_DATA lgAccV;

void gyroInit(void);
uint8_t gyroGet(void);
uint8_t gyroCalc(void);
void lowGTask(void);
void lgISR (void);
uint8_t getMaxThresh(void);                                  // Added by Jason Chen for preventing wrong recording , 2014.05.06 
void lgAccGetData(void);                                     // Added by Jason Chen for preventing wrong recording , 2014.05.06 

uint8_t lgAccGet(void);
uint8_t lgGyroGet(void);

#endif

/**
 * @}
 */
