/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
 
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "bsp_btn_ble.h"
#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "ble_app.h"
#include "fds_app.h"
#include "ble_lbs.h"

#include "max14676.h"
#include "vcnl3020.h"
#include "RTC_Drv.h"
#include "LSM6DS3.h"
#include "ADXL372.h"
#include "QSPI_Flash.h"
#include "Buzzer.h"
#include "Low_PWM_Leds.h"
#include "timer.h"
#include "rtc_internal.h"

/**@brief Function for application main entry.
 */
volatile static uint32_t second_timer;
static void timer_conn_tx_dic(void);
int main(void)
{
//  ret_code_t err_code;
	
  //Initialize.
  //log_init();
    leds_init();
    timers_init();
    gpio_interrupt_init();	  
    BRIGHT_LED_OFF();
    SENS_POWER_OFF();	
	  nrf_delay_ms(10);
	  SENS_POWER_ON();	
	  nrf_delay_ms(10);
	  power_management_init();
	
    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();
	  peer_manager_init();	
 #if 1   
    // **************** Peripherals Initialization *******************//		
		twi_init();                                                           // I2C
		nrf_delay_ms(10);
		max14676_init();                                                      // Charging Circuit
		nrf_delay_ms(10);
	  prox_init();                                                          // Prox init
		BRIGHT_LED_OFF();
				
		Initialize_RTC();                                                     // External RTC S-35392A
		
		LSM6DS3_Init();                                                       // Low G acccelerometer
		
    Buzzer_Init();    
		low_pwm_leds_init();
    White_Led(LED_ON);
    nrf_delay_ms(100);
    White_Led(LED_OFF);

	//lbs_timer_create();
		
		QSPI_Flash_Init();                                                        // QSPI initialization, configure the memory
		ADXL372_Init();
		
    accurate_timer_init();
		rtc_internal_init();
		// **************** Peripherals Initialization *******************//		
#else
    lbs_timer_create();
#endif		

#if MYFLASH_FDS_ENABLE
    fds_app_init();
#endif
    lbs_timer_create();
    // Start execution.
    NRF_LOG_INFO("GFT4 Sensor Started.\r\n");
		
    //m_test_params.erase_bonds = true;
    advertising_start(m_test_params.erase_bonds);
    if(m_test_params.erase_bonds)
		{
		  m_test_params.erase_bonds = false;
#if MYFLASH_FDS_ENABLE			
    //gft4_config_update();
      update_fds_begin();			
#endif			
		}	

		get_peer_ble_addr(__LINE__);
  	//if(m_test_params.timer_enable)
		//{
		//	app_timer_enable(m_test_params.timer_enable);
		//}		
		
    // Enter main loop. 			
		//ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_X_H, 0x52);
		//TestValue = ADXL372_SPIReadReg(ADXL372_THRESH_ACT_X_H);		
		//ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_X_H, 0x00);
    for (;;)
    {
#if MYFLASH_FDS_ENABLE
			fds_process();
		//fds_manage();
#endif						
      idle_state_handle();
			timer_conn_tx_dic();
		}
}

static uint8_t tx_dic_stm = 0;
static uint8_t conn_fail_cnt = 0;
static void timer_conn_tx_dic(void)
{
	ret_code_t err_code;	
	static bool led_toggle;
	static uint32_t dealy_count = 0;
	static uint16_t radom_dealy_time;
	static uint32_t conn_count = 0;
	
	if(second_timer != timer_1_ms)			
	{
	  second_timer = timer_1_ms;
    if(timer_tx_dic_enabled)
  	{			
			dealy_count++;
			switch(tx_dic_stm)
			{
				case 0:
					if(ble_connection == 0)
					{
						dealy_count = 0;
						//NRF_LOG_DEBUG("===>reconnection==========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
						led_toggle = !led_toggle;
						Blue_Led(led_toggle?LED_ON:LED_OFF);
					//advertising_start(false);
						err_code = re_connect(__LINE__);
						if(err_code != NRF_SUCCESS)
						{
              ble_connection = 0xFF;							
							tx_dic_stm = 1;
							break;
						}
						tx_dic_stm = 1;
						ble_connection = 0xFF;
					}
					else if(ble_connection == 1)
					{
						tx_dic_stm = 3;
					}
					break;
				case 1:
          if(ble_connection == 1)
					{
						NRF_LOG_DEBUG("===>Reconnection OK==========================>dealy_count = %d, Line:%d", dealy_count, __LINE__);
            err_code = lbs_data_send();
						if(err_code == NRF_SUCCESS)
						{
							tx_dic_stm = 2;
						}
						else
						{
						  NRF_LOG_BLUE("===>data tx fails      ==========================>...%d, Line:%d"DEFAULT_COLOR, tx_success, __LINE__);
							tx_dic_stm = 0;
						}						
					}
					else if(ble_connection == 0xFE)
					{
						radom_dealy_time = rand()%10;
						if(radom_dealy_time < 3)
						{
							radom_dealy_time = 3;
							radom_dealy_time = radom_dealy_time*100;
						}
						else
						{
							radom_dealy_time = radom_dealy_time *100;
						}
						if(conn_count > radom_dealy_time)
						{
							conn_count = 0;
							tx_dic_stm = 0;
							ble_connection = 0;							
						}
					}
					else if(ble_connection == 0)
					{
						tx_dic_stm = 0;
						NRF_LOG_BLUE("===>Connection fails, fail count: %d, Line: %d"DEFAULT_COLOR, conn_fail_cnt, __LINE__);
					}
						
					break;
				case 2:
          if(tx_success)
		      {
						NRF_LOG_BLUE("===>data tx in success ==========================>dealy_count = %d, Line:%d"DEFAULT_COLOR, dealy_count, __LINE__);
						tx_dic_stm = 3;
          }					
          break;
				case 3:
					if(ble_connection == 1)
					{
						NRF_LOG_GREEN("===>Call disconnect...........dealy_count: %d"DEFAULT_COLOR, dealy_count);
						err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
						if(err_code == NRF_SUCCESS)
						{
							tx_dic_stm = 4;
						}						
						else if(err_code == NRF_ERROR_INVALID_STATE)
						{
							NRF_LOG_RED("===>Disconnection in progress or link has not been established..."DEFAULT_COLOR);
						}
						else
						{
							NRF_LOG_RED("===>sd_ble_gap_disconnect() err_code = 0x%x"DEFAULT_COLOR, err_code);
						}
					}
					else if((ble_connection == 0)||(ble_connection == 2))
					{
						ble_connection = 0;
						tx_dic_stm = 0;
					}
          break;
				case 4:
					if(ble_connection == 0)
					{
						tx_dic_stm = 5;
						NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully....,dealy_count = %d", dealy_count);
					}
          else if(ble_connection == 2)
					{						
						tx_dic_stm = 5;
					}
          break;
				case 5:
					if(dealy_count > 4999)                 // delay 4*125 = 500 ms
					{
						NRF_LOG_DEBUG("===>The disconnection procedure has been started successfully....,dealy_count = %d, %d\r\n", dealy_count, __LINE__);
						tx_dic_stm = 0;
						dealy_count = 0;
					}
					break;
				default:
					tx_dic_stm = 0;				  
					break;
			}
		}
	}
}







/**
 * @}
 */

