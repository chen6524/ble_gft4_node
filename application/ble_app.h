/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
 
#ifndef __BLE_APP_H__
#define __BLE_APP_H__

#include <stdbool.h>
#include "nrf_delay.h"

#include "ble_gap.h"
#include "fds.h"

typedef enum
{
	SYSTEM_RESET        = 0x01,
  ERASE_IN_START      = 0x02, 
	DELETE_ALL_CONFIG   = 0x03,
	TIMER_ENABLE        = 0x04,
	GFT4_DISCONNECT     = 0x05,
	GFT4_CONNECT        = 0x06,
  GFT_MODE            = 0x71,
	ERASE_BONDED        = 0x72,	
} gft4_rx_command_t;

extern bool timer_tx_dic_enabled;
extern uint16_t m_conn_handle;
extern volatile uint8_t ble_connection;
char const * phy_str(ble_gap_phys_t phys);
typedef struct
{
    uint8_t        phy_select;		
    ble_gap_phys_t phys;                       /**< Preferred PHYs. */
    bool           erase_bonds;	
	  bool           timer_enable;
	  uint8_t        mode;
} test_params_t;

extern test_params_t m_test_params;
extern uint8_t device_full_name[12 + 5];

void leds_init(void);
void gpio_interrupt_init(void);
void timers_init(void);
void lbs_timer_create(void);
void heart_rate_timer_shot(void);

void advertising_start(bool erase_bonds);
void gap_params_init(void);
void gatt_init(void);
void peer_manager_init(void);
void advertising_init(void);
void services_init(void);
void conn_params_init(void);
void ble_stack_init(void);
void log_init(void);
void power_management_init(void);
void idle_state_handle(void);
void get_peer_ble_addr(uint32_t line);
ret_code_t re_connect(uint32_t line);
ret_code_t lbs_data_send(void);

void app_timer_enable(bool timer_enable);

__STATIC_INLINE void nrf_mydelay_ms(uint32_t ms_time)
{
    if (ms_time == 0)
    {
        return;
    }

    do {
        nrf_delay_us(1000);
			  (void) sd_app_evt_wait();
    } while (--ms_time);
}
#endif

/**
 * @}
 */
