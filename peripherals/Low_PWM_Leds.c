/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
/** @file
 * @defgroup BuzzerLeds.c
 *
 * This file contains the source code for a sample application using PWM
 *
 */

#include <stdbool.h>
#include "max14676.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "app_pwm.h"
#include "low_power_pwm.h"
#include "nrf_delay.h" 
#include "Low_PWM_Leds.h"

/*Ticks before change duty cycle of each LED*/
#define TICKS_BEFORE_CHANGE_0   500
#define TICKS_BEFORE_CHANGE_1   400

static low_power_pwm_t low_power_pwm_0;
static low_power_pwm_t low_power_pwm_1;
static low_power_pwm_t low_power_pwm_2;

#if 0
/**
 * @brief Function to be called in timer interrupt.
 *
 * @param[in] p_context     General purpose pointer (unused).
 */
static void pwm_handler(void * p_context)
{
    uint8_t new_duty_cycle;
    static uint16_t led_0, led_1;
    uint32_t err_code;
    UNUSED_PARAMETER(p_context);

    low_power_pwm_t * pwm_instance = (low_power_pwm_t*)p_context;

    if (pwm_instance->bit_mask == BSP_LED_0_MASK)
    {
        led_0++;

        if (led_0 > TICKS_BEFORE_CHANGE_0)
        {
            new_duty_cycle = pwm_instance->period - pwm_instance->duty_cycle;
            err_code = low_power_pwm_duty_set(pwm_instance, new_duty_cycle);
            led_0 = 0;
            APP_ERROR_CHECK(err_code);
        }
    }
    else if (pwm_instance->bit_mask == BSP_LED_1_MASK)
    {
        led_1++;

        if (led_1 > TICKS_BEFORE_CHANGE_1)
        {
            new_duty_cycle = pwm_instance->period - pwm_instance->duty_cycle;
            err_code = low_power_pwm_duty_set(pwm_instance, new_duty_cycle);
            led_1 = 0;
            APP_ERROR_CHECK(err_code);
        }
    }
    else
    {
        /*empty else*/
    }
}
#endif

/**
 * @brief Function to initalize low_power_pwm instances.
 *
 */
void low_pwm_leds_init(void)
{
    uint32_t err_code;
    low_power_pwm_config_t low_power_pwm_config;

    APP_TIMER_DEF(lpp_timer_0);
    low_power_pwm_config.active_high    = false;
    low_power_pwm_config.period         = 220;
    low_power_pwm_config.bit_mask       = BSP_LED_0_MASK;                            // RED
    low_power_pwm_config.p_timer_id     = &lpp_timer_0;
    low_power_pwm_config.p_port         = NRF_GPIO;

    err_code = low_power_pwm_init((&low_power_pwm_0), &low_power_pwm_config, NULL);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_0, LED_OFF);
    APP_ERROR_CHECK(err_code);

    APP_TIMER_DEF(lpp_timer_1);
    low_power_pwm_config.active_high    = false;
    low_power_pwm_config.period         = 220;
    low_power_pwm_config.bit_mask       = BSP_LED_1_MASK;                           // GREEN
    low_power_pwm_config.p_timer_id     = &lpp_timer_1;
    low_power_pwm_config.p_port         = NRF_GPIO;

    err_code = low_power_pwm_init((&low_power_pwm_1), &low_power_pwm_config, NULL);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_1, LED_OFF);
    APP_ERROR_CHECK(err_code);

    APP_TIMER_DEF(lpp_timer_2);
    low_power_pwm_config.active_high    = false;
    low_power_pwm_config.period         = 220;
    low_power_pwm_config.bit_mask       = BSP_LED_2_MASK;                          // BLUE
    low_power_pwm_config.p_timer_id     = &lpp_timer_2;
    low_power_pwm_config.p_port         = NRF_GPIO;

    err_code = low_power_pwm_init((&low_power_pwm_2), &low_power_pwm_config, NULL);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_duty_set(&low_power_pwm_2, LED_OFF);
    APP_ERROR_CHECK(err_code);

    err_code = low_power_pwm_start((&low_power_pwm_0), low_power_pwm_0.bit_mask);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_start((&low_power_pwm_1), low_power_pwm_1.bit_mask);
    APP_ERROR_CHECK(err_code);
    err_code = low_power_pwm_start((&low_power_pwm_2), low_power_pwm_2.bit_mask);
    APP_ERROR_CHECK(err_code);
}


void Red_Led(uint8_t On_off )
{
	uint32_t err_code;
  err_code = low_power_pwm_duty_set(&low_power_pwm_0, On_off);
  APP_ERROR_CHECK(err_code);
}

void Green_Led(uint8_t On_off )
{
	uint32_t err_code;
  err_code = low_power_pwm_duty_set(&low_power_pwm_1, On_off);
  APP_ERROR_CHECK(err_code);	
}

void Blue_Led(uint8_t On_off )
{
	uint32_t err_code;
  err_code = low_power_pwm_duty_set(&low_power_pwm_2, On_off);
  APP_ERROR_CHECK(err_code);	
}

void White_Led(uint8_t On_off )
{
	uint32_t err_code;
  err_code = low_power_pwm_duty_set(&low_power_pwm_0, On_off);
  APP_ERROR_CHECK(err_code);

  err_code = low_power_pwm_duty_set(&low_power_pwm_1, On_off);
  APP_ERROR_CHECK(err_code);

  err_code = low_power_pwm_duty_set(&low_power_pwm_2, On_off);
  APP_ERROR_CHECK(err_code);	
}

void Leds_Stop(void)
{
	uint32_t err_code;
  err_code = low_power_pwm_duty_set(&low_power_pwm_0, LED_OFF);
  APP_ERROR_CHECK(err_code);

  err_code = low_power_pwm_duty_set(&low_power_pwm_1, LED_OFF);
  APP_ERROR_CHECK(err_code);

  err_code = low_power_pwm_duty_set(&low_power_pwm_2, LED_OFF);
  APP_ERROR_CHECK(err_code);	 
	
	err_code = low_power_pwm_stop(&low_power_pwm_0);
	APP_ERROR_CHECK(err_code);

	err_code = low_power_pwm_stop(&low_power_pwm_1);
	APP_ERROR_CHECK(err_code);

	err_code = low_power_pwm_stop(&low_power_pwm_2);
	APP_ERROR_CHECK(err_code);	
}


/** @} */



