/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _VCNL3020_H_
#define _VCNL3020_H_

#include <stdio.h>
#include <stdint.h>

#define PROX_INT          27                                      // interrupt pin of RTC, which will be trggered one second or one minute
#define PROX_INT_PULL     NRF_GPIO_PIN_PULLUP
#define PROX_INT_ENABLE   0

#define VCNL3020_DEV_ADDR                       (0x13)
#define VCNL3020_COMMAND_REG0                   0x80
#define VCNL3020_ID_REVISION_REG1               0x81
#define VCNL3020_PROX_RATE_REG2                 0x82
#define VCNL3020_IRRED_CUR_REG3                 0x83
#define VCNL3020_PROX_RESULT_REG7               0x87
#define VCNL3020_PROX_RESULT_REG8               0x88
#define VCNL3020_INT_CTRL_REG9                  0x89
#define VCNL3020_LOW_THRESH_REG10               0x8A
#define VCNL3020_LOW_THRESH_REG11               0x8B
#define VCNL3020_HIGH_THRESH_REG12              0x8C
#define VCNL3020_HIGH_THRESH_REG13              0x8D
#define VCNL3020_INT_STATUS_REG14               0x8E
#define VCNL3020_PROX_MOD_TIMING_ADJ_REG15      0x8F

#define CONFIG_LOCK                             (1 << 7)
#define PROX_DATA_RDY                           (1 << 5)
#define PROX_OD                                 (1 << 3)
#define PROX_EN                                 (1 << 1)
#define SELFTIMED_EN                            (1 << 0)

#define PROX_RATE_MASK                          0x03
#define PROX_RATE_1_95000                       0x00
#define PROX_RATE_3_90625                       0x01
#define PROX_RATE_7_81250                       0x02
#define PROX_RATE_16_6250                       0x03
#define PROX_RATE_31_2500                       0x04
#define PROX_RATE_62_5000                       0x05
#define PROX_RATE_125_000                       0x06
#define PROX_RATE_250_000                       0x07

#define INT_COUNT_EXCEED_01                      (0x00 << 5)
#define INT_COUNT_EXCEED_02                      (0x01 << 5)
#define INT_COUNT_EXCEED_04                      (0x02 << 5)
#define INT_COUNT_EXCEED_08                      (0x03 << 5)
#define INT_COUNT_EXCEED_16                      (0x04 << 5)
#define INT_COUNT_EXCEED_32                      (0x05 << 5)
#define INT_COUNT_EXCEED_64                      (0x06 << 5)
#define INT_COUNT_EXCEED_128                     (0x07 << 5)
#define INT_PROX_READY_EN                        ( 1 << 3)
#define INT_THRES_EN                             ( 1 << 1)
#define INT_THRES_SEL                            ( 1 << 0)

#define INT_PROX_READY                           ( 1 << 3)
#define INT_TH_LOW                               ( 1 << 1)
#define INT_TH_HI                                ( 1 << 0)

void prox_int_event_handler(uint8_t pin_no, uint8_t int_action);
void vcnl3020_WriteReg(uint8_t regAddr,uint8_t regValue);
uint32_t vcnl3020_ReadReg(uint8_t regAddr, uint8_t* ptrVal);
uint8_t vcnl3020_Read(uint8_t regAddr);
uint8_t vcnl3020_Read01(uint8_t regAddr);
void prox_init(void);
void prox_process(void);

#endif
