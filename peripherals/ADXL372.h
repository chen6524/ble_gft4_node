/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _ADXL372_H_
#define _ADXL372_H_

#include <stdint.h>

#define INT1_HG                46                                                   // interrupt pin of HG Accelerometer
#define INT1_HG_PULL           NRF_GPIO_PIN_PULLUP
#define INT1_HG_ENABLE         0

#define ADXL_DUMMY_BYTE  0xA5

#define ADXL372_DEVID_AD                      0x00
#define ADXL372_DEVID_MST                     0x01
#define ADXL372_PARTID                        0x02
#define ADXL372_REVID                         0x03
#define ADXL372_STATUS                        0x04
#define ADXL372_STATUS2                       0x05
#define ADXL372_FIFO_ENTRIES2                 0x06
#define ADXL372_FIFO_ENTRIES                  0x07
#define ADXL372_XDATA_H                       0x08
#define ADXL372_XDATA_L                       0x09
#define ADXL372_YDATA_H                       0x0A
#define ADXL372_YDATA_L                       0x0B
#define ADXL372_ZDATA_H                       0x0C
#define ADXL372_ZDATA_L                       0x0D
#define ADXL372_MAXPEAK_X_H                   0x15
#define ADXL372_MAXPEAK_X_L                   0x16
#define ADXL372_MAXPEAK_Y_H                   0x17
#define ADXL372_MAXPEAK_Y_L                   0x18
#define ADXL372_MAXPEAK_Z_H                   0x19
#define ADXL372_MAXPEAK_Z_L                   0x1A
#define ADXL372_OFFSET_X                      0x20
#define ADXL372_OFFSET_Y                      0x21
#define ADXL372_OFFSET_Z                      0x22
#define ADXL372_THRESH_ACT_X_H                0x23
#define ADXL372_THRESH_ACT_X_L                0x24
#define ADXL372_THRESH_ACT_Y_H                0x25
#define ADXL372_THRESH_ACT_Y_L                0x26
#define ADXL372_THRESH_ACT_Z_H                0x27
#define ADXL372_THRESH_ACT_Z_L                0x28
#define ADXL372_TIME_ACT                      0x29
#define ADXL372_THRESH_INACT_X_H              0x2A
#define ADXL372_THRESH_INACT_X_L              0x2B
#define ADXL372_THRESH_INACT_Y_H              0x2C
#define ADXL372_THRESH_INACT_Y_L              0x2D
#define ADXL372_THRESH_INACT_Z_H              0x2E
#define ADXL372_THRESH_INACT_Z_L              0x2F
#define ADXL372_TIME_INACT_H                  0x30
#define ADXL372_TIME_INACT_L                  0x31
#define ADXL372_THRESH_ACT2_X_H               0x32
#define ADXL372_THRESH_ACT2_X_L               0x33
#define ADXL372_THRESH_ACT2_Y_H               0x34
#define ADXL372_THRESH_ACT2_Y_L               0x35
#define ADXL372_THRESH_ACT2_Z_H               0x36
#define ADXL372_THRESH_ACT2_Z_L               0x37
#define ADXL372_HPF                           0x38
#define ADXL372_FIFO_SAMPLES                  0x39
#define ADXL372_FIFO_CTL                      0x3A
#define ADXL372_INT1_MAP                      0x3B
#define ADXL372_INT2_MAP                      0x3C
#define ADXL372_TIMING                        0x3D
#define ADXL372_MEASURE                       0x3E
#define ADXL372_POWER_CTL                     0x3F
#define ADXL372_SELF_TEST                     0x40
#define ADXL372_RESET                         0x41
#define ADXL372_FIFO_DATA                     0x42

//ADXL372 DEVID_AD                            0x00
#define DEVICE_ID                             0xAD

//ADXL372 DEVID_MST                           0x01
#define DEVID_MST                             0x1D

//ADXL372 PARTID                              0x02
#define DEVID_PRODUCT                         0xFA

//ADXL372 REVID                               0x03
#define REVID                                 0x02

//ADXL372 STATUS                              0x04    (Read Only)
#define ERR_USER_REGS                         (1 << 7)
#define AWAKE                                 (1 << 6)
#define USER_NVM_BUSY                         (1 << 5)
#define FIFO_OVR                              (1 << 3)
#define FIFO_FULL                             (1 << 2)
#define FIFO_RDY                              (1 << 1)
#define DATA_RDY                              (1 << 0)

//ADXL372 STATUS2                             0x05    (Read Only)
#define ACTIVITY2                             (1 << 6)
#define ACTIVITY                              (1 << 5)
#define INACT                                 (1 << 4)

//ADXL372 HPF Setting                         0x38
#define HPF_15_24HZ_BY_3200HZ                 0x00
#define HPF_07_79HZ_BY_3200HZ                 0x01
#define HPF_03_94HZ_BY_3200HZ                 0x02
#define HPF_01_98HZ_BY_3200HZ                 0x03

//ADXL372 FIFO CTL                            0x3A
#define FIFO_XYZ_PEAK_ALL                     (7 << 3)
#define FIFO_XYZ_ACC_DATA                     (0 << 3)
#define FIFO_BYPASS_MODE                      (0 << 1)
#define FIFO_STREAM_MODE                      (1 << 1)
#define FIFO_TRIGGER_MODE                     (2 << 1)
#define FIFO_OLDEST_SAVED_MODE                (3 << 1)

//ADXL372 INT1_MAP                            0x3B
#define INT1_LOW_ACTIVE                       (1 << 7)
#define AWAKE_INT1                            (1 << 6)
#define ACTIVE_INT1                           (1 << 5)
#define INACTIVE_INT1                         (1 << 4)
#define FIFO_OVR_INT1                         (1 << 3)
#define FIFO_FULL_INT1                        (1 << 2)
#define FIFO_RDY_INT1                         (1 << 1)
#define DATA_RDY_INT1                         (1 << 0)

//ADXL372 TIMING                              0x3D
#define ODR_0400                              (0 << 5)
#define ODR_0800                              (1 << 5)
#define ODR_1600                              (2 << 5)
#define ODR_3200                              (3 << 5)
#define ODR_6400                              (4 << 5)
#define WAKEUP_RATE_52MS                      (0 << 2)
#define WAKEUP_RATE_104MS                     (1 << 2)
#define WAKEUP_RATE_208MS                     (2 << 2)
#define WAKEUP_RATE_512MS                     (3 << 2)
#define WAKEUP_RATE_2048MS                    (4 << 2)
#define WAKEUP_RATE_4096MS                    (5 << 2)
#define WAKEUP_RATE_8192MS                    (5 << 2)
#define WAKEUP_RATE_24576MS                   (5 << 2)

//ADXL372 MEASUREMENT                         0x3E
#define AUTOSLEEP                             (1 << 6)
#define LINKLOOP_DEFAULT_MODE                 (0 << 4)
#define LINKLOOP_LINKED_MODE                  (1 << 4)
#define LINKLOOP_LOOP_MODE                    (2 << 4)
#define LOW_NOISE                             (1 << 3)
#define BANDWIDTH_0200HZ                      0x00
#define BANDWIDTH_0400HZ                      0x01
#define BANDWIDTH_0800HZ                      0x02
#define BANDWIDTH_1600HZ                      0x03
#define BANDWIDTH_3200HZ                      0x04

//ADXL372 POWER_CTL                           0x3F
#define INSTANT_ON_THRESH_LOW                 (0 << 5)
#define INSTANT_ON_THRESH_HIGH                (1 << 5)
#define FILTER_SETTLE_370MS                   0x00
#define FILTER_SETTLE_16MS                    0x10
#define LPF_ENABLE                            (1 << 3)
#define HPF_ENABLE                            (1 << 2)
#define STANDBY_MODE                          (0 << 0)
#define WAKE_UP_MODE                          (1 << 0)
#define INSTANT_ON_MODE                       (2 << 0)
#define MEASUREMENT_MODE                      (3 << 0)

void int_hg_event_handler(uint8_t pin_no, uint8_t int_action);
void ADXL372_Init(void);
uint8_t ADXL372_SPIReadReg(uint8_t regAddr);
void ADXL372_SPIWriteReg(uint8_t regAddr, uint8_t regValue);
void ADXL372_SPIBufferRead(uint8_t* pBuffer, uint8_t readAddr, uint8_t numByteToRead);

void ADXL372_MAXPEAK_DataRead(int16_t *pBuffer);
void ADXL372_FIFO_DataRead(uint8_t *pBuffer, uint8_t data_len);
void hg_process(void);
#endif

