/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _LOW_PWM_LEDS_H_
#define _LOW_PWM_LEDS_H_

#include <stdint.h>
#include <stdbool.h>

#define LED_ON             10
#define LED_OFF            0

void low_pwm_leds_init(void);

void Red_Led(uint8_t On_off );
void Green_Led(uint8_t On_off );
void Blue_Led(uint8_t On_off );
void White_Led(uint8_t On_off );
void Leds_Stop(void);

#endif

