/**
 * Copyright (c) 2015 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Blinky Sample Application main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */

#ifndef __FDS_APP_H__
#define __FDS_APP_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "fds.h"
#include "ble_gap.h"

#define MYFLASH_FDS_ENABLE            1

/* File ID and Key used for the configuration record. */

#define CONFIG_FILE     (0xB111)
#define CONFIG_REC_KEY  (0x7010)

typedef enum
{
	BONDING_MODE        = 0x01,
  WORKING_MODE        = 0x02, 
} gateway_mode_t;

/* A dummy structure to save in flash. */
typedef struct
{
    uint32_t        config0;	
	  char            device_name[20];
    bool            erase_bonds;	  
	  bool            timer_enable;	
	  uint8_t         phy_select;
	  ble_gap_phys_t  phy_config;
	  uint8_t         mode;
} configuration_t;

void fds_app_init(void);
void fds_manage(void);
void delete_all_begin(void);
void update_fds_begin(void);
void update_fds_pending_clear(void);
void Bonded_delete_begin(void);
//void update_fds_process(void);
void fds_process(void);
//void delete_all_process(void);
void display_fds_status(void);
void gft4_config_update(void);

#endif

/**
 * @}
 */
