/***************************************************************************
 *
 *            Copyright (c) 2016 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
  * http:  www.artaflex.com

 *
 ***************************************************************************/
#ifndef _MAX14676_H_
#define _MAX14676_H_

#include <stdio.h>
#include <stdint.h>
#include "nrf_drv_twi.h"

#define FGAUGE_INT             28                                                   // interrupt pin of Charging Circuit, 2019.11.13
#define INT_CHRG               31                                                   // interrupt pin of Charging Circuit
#define FGAUGE_INT_PULL        NRF_GPIO_PIN_NOPULL
#define INT_CHRG_PULL          NRF_GPIO_PIN_PULLUP
#define FGAUGE_INT_ENABLE      0
#define INT_CHRG_ENABLE        0

//#define ADVERTISING_LED               BSP_BOARD_LED_0                         /**< Is on when device is advertising. */  // RED 
//#define CONNECTED_LED                 BSP_BOARD_LED_1                         /**< Is on when device has connected. */   // GREEN
#define RED_LED                         BSP_BOARD_LED_0
#define GREEN_LED                       BSP_BOARD_LED_1  
#define BLUE_LED                        BSP_BOARD_LED_2                         /**< LED to be toggled with the help of the LED Button Service. */ // BLUE
#define BUZZER                          BSP_BOARD_LED_3
#define BRIGHT_LED                      BSP_BOARD_LED_4
#define SENS_POWER                      BSP_BOARD_LED_5

#define BUZZER_OFF()                    bsp_board_led_on(BUZZER)
#define BUZZER_ON()                     bsp_board_led_off(BUZZER)
#define BRIGHT_LED_OFF()                bsp_board_led_on(BRIGHT_LED)
#define BRIGHT_LED_ON()                 bsp_board_led_off(BRIGHT_LED)
#define SENS_POWER_OFF()                bsp_board_led_on(SENS_POWER)
#define SENS_POWER_ON()                 bsp_board_led_off(SENS_POWER)
//#define RED_LED_OFF()                 bsp_board_led_off(RED_LED)
//#define RED_LED_ON()                  bsp_board_led_on(RED_LED)
//#define BLUE_LED_OFF()                bsp_board_led_off(BLUE_LED)
//#define BLUE_LED_ON()                 bsp_board_led_on(BLUE_LED)

#define MAX14676_DEV_ADDR                       (0x28)
#define MAX14676_CHIP_ID                        0x00
#define MAX14676_STATUS_A                       0x02
#define MAX14676_STATUS_B                       0x03
#define MAX14676_STATUS_C                       0x04
#define MAX14676_INT_A                          0x05
#define MAX14676_INT_B                          0x06
#define MAX14676_INT_MASKA                      0x07
#define MAX14676_INT_MASKB                      0x08
#define MAX14676_ILIMCNTL_ADDR                  0x0A
#define MAX14676_CHGCNTLA                       0x0B
#define MAX14676_CHGCNTLB                       0x0C
#define MAX14676_CHGVSET                        0x0E
#define MAX14676_PCHGCNTL                       0x10
#define MAX14676_CDETCNTLB                      0x11
#define MAX14676_LDOCFG                         0x16
#define MAX14676_PWR_CFG_ADDR 					        0x1E

#define USBOK_STATUS                            0x08            //(1 << 3)
#define USBOVP_STATUS                           (1 << 5)

#define USBOVP_INT                              (1 << 6)
#define USBOK_INT                               0x08            //(1 << 3)
#define CHGSTAT_INT                             0x04            //(1 << 2)

#define USBOVP_MASK                             (1 << 6)
#define USBOK_MASK                              0x08            //(1 << 3)
#define CHGSTAT_MASK                            0x04            //(1 << 2)

#define CHARGER_OFF                             0x00
#define CHARGEING_SUSPENDED                     0x01
#define PRECHARGE_IN_PROCESS                    0x02
#define CHARGE_CONSTANT_CURRENT                 0x03
#define CHARGE_CONSTANT_VOLTAGE                 0x04
#define CHARGE_IN_PROCESS                       0x05
#define CHARGER_TIMER_DONE                      0x06
#define CHARGER_FAULT_CONDITION                 0x07


#define MAX14676_DEV_ADDR2                      (0x36)

#define MAX14676_VALRT                          0x14
#define MAX14676_STATUS 				                0x1A    //0x00

extern const nrf_drv_twi_t m_twi;

void int_chg_event_handler(uint8_t pin_no, uint8_t int_action);
void max14676_init(void);
void max14676_GetVer(void);
void EnableCHG(void);

uint8_t max14676_Read(uint8_t regAddr);
uint8_t max14676_Read01(uint8_t regAddr);
uint32_t max14676_ReadReg(uint8_t regAddr, uint8_t* ptrVal);
uint16_t max14676_Read2(uint8_t regAddr);
uint32_t max14676_ReadReg2(uint8_t regAddr, uint16_t * pReadVal);

void max14676_process(void);

uint8_t max14676_GetBatteryCharge(void);
uint16_t max14676_GetBatteryChargeVoltage( void);
void max14676_poweroff(void);

extern uint8_t max14676Exist;
extern uint8_t max14676ChargeStat;
extern uint16_t max14676Voltage;
extern uint16_t max14676Voltage_old ;                  // 2014.12.08
extern uint8_t max14676Charge ;
extern uint8_t  TestValue;
extern uint16_t data16;

extern uint32_t RCC_CSR_reg;

extern void DisableCHG(void);
extern void ReStart_Process(void);

void twi_init (void);

#endif
