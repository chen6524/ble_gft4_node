/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
/** @file
 * @defgroup BuzzerLeds.c
 *
 * This file contains the source code for a sample application using PWM
 *
 */

#include <stdbool.h>
#include "max14676.h"
#include "Buzzer.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "app_pwm.h"
#include "nrf_drv_pwm.h"
#include "nrf_delay.h" 

APP_PWM_INSTANCE(PWM1,1);                                     // Create the instance "PWM1" using TIMER1.
//static nrf_drv_pwm_t m_pwm2 = NRF_DRV_PWM_INSTANCE(2);

static volatile bool ready_flag;            // A flag indicating PWM status.

void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}

void Buzzer_Init(void)
{
    ret_code_t err_code;
	  //uint32_t value;

    /* 2-channel PWM, 200Hz, output on DK LED pins. */
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(625L, BSP_LED_3);//BSP_LED_1);
	  //app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, BSP_LED_0, BSP_LED_3);//BSP_LED_1);

    /* Switch the polarity of the second channel. */
    pwm1_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;

    /* Initialize and enable PWM. */
    err_code = app_pwm_init(&PWM1,&pwm1_cfg,NULL);//pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
    app_pwm_enable(&PWM1);
	
	  //ready_flag = false;
    /* Set the duty cycle - keep trying until PWM is ready... */
    //app_pwm_channel_duty_set(&PWM1, 0, 25);
    /* ... or wait for callback. */
     //while (!ready_flag);	
}

void Buzzer_On(void)
{
	app_pwm_channel_duty_set(&PWM1, 0, 25);
}

void Buzzer_Off(void)
{
	app_pwm_channel_duty_set(&PWM1, 0, 0);
}

void Buzzer_Stop(void)
{
	app_pwm_disable(&PWM1);
	app_pwm_uninit(&PWM1);
}

void Buzzer_Beep(void)
{
	app_pwm_channel_duty_set(&PWM1, 0, 25);
	nrf_delay_ms(30);
  app_pwm_channel_duty_set(&PWM1, 0, 0);
}


/** @} */



