/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
/** @file
 * @defgroup QSPI_Flash.c
 *
 * This file contains the source code for a sample application using QSPI
 *
 */

#include <stdio.h>
#include "max14676.h"
#include "QSPI_Flash.h"
#include "nrf_drv_qspi.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_spi.h"

#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define QSPI_STD_CMD_WRSR   0x01
#define QSPI_STD_CMD_RSTEN  0x66
#define QSPI_STD_CMD_RST    0x99

#define QSPI_TEST_DATA_SIZE 256

#define INTERRUPT_MODE      1
#if INTERRUPT_MODE
#define WAIT_FOR_PERIPH() do { \
        while (!m_finished) {} \
        m_finished = false;    \
    } while (0)
#else
#define WAIT_FOR_PERIPH()
#endif
static volatile bool m_finished = false;
//static uint8_t m_buffer_tx[QSPI_TEST_DATA_SIZE];
//static uint8_t m_buffer_rx[QSPI_TEST_DATA_SIZE];

static void qspi_handler(nrf_drv_qspi_evt_t event, void * p_context)
{
    UNUSED_PARAMETER(event);
    UNUSED_PARAMETER(p_context);
    m_finished = true;
}

static void configure_memory()
{
    uint8_t temporary = 0x40;
    uint32_t err_code;
    nrf_qspi_cinstr_conf_t cinstr_cfg = {
        .opcode    = QSPI_STD_CMD_RSTEN,
        .length    = NRF_QSPI_CINSTR_LEN_1B,
        .io2_level = true,
        .io3_level = true,
        .wipwait   = true,
        .wren      = true
    };

    // Send reset enable
    err_code = nrf_drv_qspi_cinstr_xfer(&cinstr_cfg, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    // Send reset command
    cinstr_cfg.opcode = QSPI_STD_CMD_RST;
    err_code = nrf_drv_qspi_cinstr_xfer(&cinstr_cfg, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    // Switch to qspi mode
    cinstr_cfg.opcode = QSPI_STD_CMD_WRSR;
    cinstr_cfg.length = NRF_QSPI_CINSTR_LEN_2B;
    err_code = nrf_drv_qspi_cinstr_xfer(&cinstr_cfg, &temporary, NULL);
    APP_ERROR_CHECK(err_code);
}

void QSPI_Flash_Init(void)
{
    uint32_t err_code;
	
		nrfx_qspi_uninit();
			
    nrf_drv_qspi_config_t config = NRF_DRV_QSPI_DEFAULT_CONFIG;
#if INTERRUPT_MODE		
		err_code = nrf_drv_qspi_init(&config, qspi_handler, NULL);
#else		
    err_code = nrf_drv_qspi_init(&config, NULL,         NULL);
#endif		
    APP_ERROR_CHECK(err_code);

    configure_memory();
#if WRITE_READ_TEST
    err_code = nrf_drv_qspi_erase(NRF_QSPI_ERASE_LEN_64KB, 0);
    APP_ERROR_CHECK(err_code);
    WAIT_FOR_PERIPH();
    NRF_LOG_INFO("Process of erasing first block start");

    err_code = nrf_drv_qspi_write(m_buffer_tx, QSPI_TEST_DATA_SIZE, 0);
    APP_ERROR_CHECK(err_code);
    WAIT_FOR_PERIPH();
    NRF_LOG_INFO("Process of writing data start");

    err_code = nrf_drv_qspi_read(m_buffer_rx, QSPI_TEST_DATA_SIZE, 0);
		APP_ERROR_CHECK(err_code);
    WAIT_FOR_PERIPH();
    NRF_LOG_INFO("Data read");

    NRF_LOG_INFO("Compare...");
    if (memcmp(m_buffer_tx, m_buffer_rx, QSPI_TEST_DATA_SIZE) == 0)
    {
        BRIGHT_LED_OFF();
			  BLUE_LED_ON();
    }
    else
    {
        BRIGHT_LED_ON();
    }
#endif
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SectorErase
* Description    : Erases the specified FLASH sector.
* Input          : SectorAddr: address of the sector to erase.
* Output         : None
* Return         : None
*******************************************************************************/
void QPSI_FLASH_SectorErase4K(uint32_t SectorAddr)
{
	uint32_t err_code;
	
  err_code = nrf_drv_qspi_erase(NRF_QSPI_ERASE_LEN_4KB, SectorAddr);
  APP_ERROR_CHECK(err_code);
  WAIT_FOR_PERIPH();	
}

void QPSI_FLASH_SectorErase64K(uint32_t SectorAddr)
{
	uint32_t err_code;
	
  err_code = nrf_drv_qspi_erase(NRF_QSPI_ERASE_LEN_64KB, SectorAddr);
  APP_ERROR_CHECK(err_code);
  WAIT_FOR_PERIPH();	
}

/*******************************************************************************
* Function Name  : SPI_FLASH_PageWrite
* Description    : Writes more than one byte to the FLASH with a single WRITE
*                  cycle(Page WRITE sequence). The number of byte can't exceed
*                  the FLASH page size.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH,
*                    must be equal or less than "SPI_FLASH_PageSize" value.
* Output         : None
* Return         : None
*******************************************************************************/
//extern void Cpu_Delay100US(word us100);

void QSPI_FLASH_PageWrite(uint8_t* WriteAddr, uint32_t Write_Addr, uint16_t NumByteToWrite)
{
	uint32_t err_code;
  
	err_code = nrf_drv_qspi_write(WriteAddr, NumByteToWrite, Write_Addr);
  APP_ERROR_CHECK(err_code);
  WAIT_FOR_PERIPH();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferRead
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void QSPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
	uint32_t err_code;

  err_code = nrf_drv_qspi_read(pBuffer, NumByteToRead, ReadAddr);
	APP_ERROR_CHECK(err_code);
  WAIT_FOR_PERIPH();
}

/** @} */



