/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#include <stdio.h>
#include "max14676.h"
#include "rtc_internal.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_rtc.h"
#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28
#include "Low_PWM_Leds.h"

/** @file
 * @defgroup rtc_internal.c
 +
 *
 * This file contains the source code for a sample application using timer
 *
 */
 #define COMPARE_COUNTERTIME  (5UL)                                        /**< Get Compare event COMPARE_TIME seconds after the counter starts from 0. */
 
 // RTC0 for BLE softdevice
 // RTC1 for app_timer
 // RTC2 just only for application
 const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2); /**< Declaring an instance of nrf_drv_rtc for RTC0. */
 #define  RTC2_PRESCALER        512                                        // RTC2 Tick frequence = 32768/RTC_PRESCALER = 64Hz
 
 /** @brief: Function for handling the RTC0 interrupts.
 * Triggered on TICK and COMPARE0 match.
 */
 uint32_t rtc_second;
void rtc_internal_handler(nrf_drv_rtc_int_type_t int_type)
{
	static uint32_t rtc_cnt = 0;
	static uint32_t rtc_64hz_count = 0;
	
  if (int_type == NRF_DRV_RTC_INT_TICK)
  {		
		if(rtc_cnt == 0)
		{
			//BRIGHT_LED_ON();                                  // second rate
			Green_Led(LED_ON);
      rtc_cnt++;			
		}
		else if(rtc_cnt == 1)
		{
		  //BRIGHT_LED_OFF();
			Green_Led(LED_OFF);
			rtc_cnt++;
		}
		else if((rtc_cnt > 1)&&(rtc_cnt < RTC_INPUT_FREQ/RTC2_PRESCALER))
		{
			rtc_cnt++;
		}
		else
		{
			rtc_cnt = 0;
		}
		
		rtc_64hz_count++;
		if(rtc_64hz_count > 64*5 - 1)
		{
			rtc_64hz_count = 0;
			//bsp_board_led_invert(BSP_BOARD_LED_2);
			rtc_second++;
		}				
  }
	else if (int_type == NRF_DRV_RTC_INT_COMPARE0)
  {
		//Blue_Led(LED_ON);
	}

}


void rtc_internal_init(void)
{
    uint32_t err_code;

    //Initialize RTC instance
    nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
    config.prescaler = RTC2_PRESCALER - 1;//(4096 - 1);                                                            // 32768/RTC_PRESCALER Hz, Max value = 4096, so all setup value <= 4095
    err_code = nrf_drv_rtc_init(&rtc, &config, rtc_internal_handler);
    APP_ERROR_CHECK(err_code);

    //Enable tick event & interrupt
    nrf_drv_rtc_tick_enable(&rtc,true);

    //Set compare channel to trigger interrupt after COMPARE_COUNTERTIME seconds
    err_code = nrf_drv_rtc_cc_set(&rtc,0, COMPARE_COUNTERTIME * (RTC_INPUT_FREQ/RTC2_PRESCALER),true);                      // COMPARE_COUNTERTIME * 8 ---> 3 Seconds
    APP_ERROR_CHECK(err_code);

    //Power on RTC instance
    nrf_drv_rtc_enable(&rtc);
}
 
 /** @} */


