/** @file
 * @defgroup max14676.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */

#include <stdio.h>
#include "max14676.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "ble_lbs.h"

#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28

#define TWI_BLOCKING_MODE               1

/* TWI instance ID. */
#if TWI0_ENABLED
#define TWI_INSTANCE_ID     0
#elif TWI1_ENABLED
#define TWI_INSTANCE_ID     1
#endif

 /* Number of possible TWI addresses. */
 #define TWI_ADDRESSES      127

static volatile bool m_tx_xfer_done = false;
static volatile bool m_rx_xfer_done = false;
static volatile bool m_txrx_xfer_done = false;
//static uint8_t m_sample;

/* TWI instance. */
const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

#if (TWI_BLOCKING_MODE == 0)
/**
 * @brief TWI events handler.
 */
 bool led_flag = false;
 //bool redled_flag = false;
void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
							m_rx_xfer_done = true;
#if 0							
  						redled_flag = !redled_flag;
							if(redled_flag)						
							  RED_LED_ON();
							else
								RED_LED_OFF();							
#endif							
            }
						else if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_TX)
						{
							m_tx_xfer_done = true;
#if 0						
  						led_flag = !led_flag;
							if(led_flag)						
							  BRIGHT_LED_ON();
							else
								BRIGHT_LED_OFF();														
#endif							
						}
						else if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_TXRX)
						{
							m_txrx_xfer_done = true;
						}            					
            break;
        default:
            break;
    }
}
#endif

/**
 * @brief TWI initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_config = {
       .scl                = GFT4_SCL_PIN,
       .sda                = GFT4_SDA_PIN,
       .frequency          = NRF_DRV_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

		//if(nrf_drv_twi_is_busy(&m_twi))
		//{
		//	nrf_drv_twi_uninit(&m_twi);
		//}		
#if TWI_BLOCKING_MODE
		err_code = nrf_drv_twi_init(&m_twi, &twi_config, NULL, NULL);
#else		
    err_code = nrf_drv_twi_init(&m_twi, &twi_config, twi_handler, NULL);
#endif		
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}

nrfx_err_t nrfx_twi_txrx(nrfx_twi_t const * p_instance,
                       uint8_t            address,
                       uint8_t *          p_data,
                       size_t             length,
                       uint8_t *          p_data1,
                       size_t             length1)
{
	  ret_code_t err_code;
    nrfx_twi_xfer_desc_t xfer_tx = NRFX_TWI_XFER_DESC_TX(address, p_data, length);
	  m_tx_xfer_done = false;
    err_code = nrfx_twi_xfer(p_instance, &xfer_tx, 1);
	  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)
	  while (m_tx_xfer_done == false);
#endif
	  nrf_delay_ms(50);
	  nrfx_twi_xfer_desc_t xfer_rx = NRFX_TWI_XFER_DESC_RX(address, p_data1, length1);
	  m_rx_xfer_done = false;
	  err_code = nrfx_twi_xfer(p_instance, &xfer_rx, 0);	  
	  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	  while (m_rx_xfer_done == false);
#endif
	  return 0;
}

void nrf_drv_twi_txrx(nrf_drv_twi_t const * p_instance,
                            uint8_t               address,
                            uint8_t *             p_data,
                            uint8_t               length,
                            uint8_t *             p_data1,
                            uint8_t               length1)
{
#if 1
    nrfx_twi_txrx(&p_instance->u.twi, address, p_data, length, p_data1, length1);
#else
	  ret_code_t err_code;
    nrfx_twi_xfer_desc_t xfer_tx = NRFX_TWI_XFER_DESC_TX(address, p_data, length);
	  m_tx_xfer_done = false;
    err_code = nrfx_twi_xfer(&(p_instance->u.twi), &xfer_tx, 1);
	  APP_ERROR_CHECK(err_code);
    #if (TWI_BLOCKING_MODE == 0)
	  while (m_tx_xfer_done == false);
    #endif	  	 
	
	  nrfx_twi_xfer_desc_t xfer_rx = NRFX_TWI_XFER_DESC_RX(address, p_data1, length1);
	  m_rx_xfer_done = false;
	  err_code = nrfx_twi_xfer(&(p_instance->u.twi), &xfer_rx, 0);	  
	  APP_ERROR_CHECK(err_code);
    #if (TWI_BLOCKING_MODE == 0)	
	  while (m_rx_xfer_done == false);
    #endif
#endif	
}


uint8_t  max14676Exist       = 0;
uint8_t  TestValue           = 0;
uint16_t data16              = 0;
uint16_t max14676Voltage     = 0x1FFF;
uint16_t max14676Voltage_old = 0x1FFF;                        // 2014.12.08
uint8_t  max14676Charge      = 0;                             //init to 50%
uint8_t  max14676ChargeStat  = 0;
uint8_t  max14676_result;

void int_chg_event_handler(uint8_t pin_no, uint8_t int_action)
{
    switch (pin_no)
    {
        case FGAUGE_INT:
				    if(int_action)
						{
							//BRIGHT_LED_ON();
						}
						else
						{
							//BRIGHT_LED_OFF();
						}
            break;
				case INT_CHRG:
            break;					
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

void max14676_WriteReg(uint8_t regAddr,uint8_t regValue) 
{
	ret_code_t err_code;
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
	
	m_tx_xfer_done = false;
  err_code = nrf_drv_twi_tx(&m_twi, MAX14676_DEV_ADDR, send_Buf, 2, false);			
  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	while (m_tx_xfer_done == false);
#endif	
}

uint32_t max14676_ReadReg(uint8_t regAddr, uint8_t* ptrVal) 
{
	ret_code_t err_code;
//  uint8_t result = 0;
  uint8_t readValue[2];
  
  m_tx_xfer_done = false;	
	err_code = nrf_drv_twi_tx(&m_twi, MAX14676_DEV_ADDR, &regAddr, 1, true);
	if(err_code != NRF_SUCCESS)
		return err_code;
#if (TWI_BLOCKING_MODE == 0)
	while (m_tx_xfer_done == false);
#endif	

	m_rx_xfer_done = false;
  err_code = nrf_drv_twi_rx(&m_twi, MAX14676_DEV_ADDR, readValue, 2);
	if(err_code != NRF_SUCCESS)
		return err_code;
#if (TWI_BLOCKING_MODE == 0)	
	while (m_rx_xfer_done == false)
	{}
#endif	
			
  *ptrVal = readValue[0];
  return  err_code; 
}

uint8_t max14676_Read(uint8_t regAddr) 
{
  ret_code_t err_code;
  uint8_t readValue[2];
   
	m_tx_xfer_done = false;
  err_code = nrf_drv_twi_tx(&m_twi, MAX14676_DEV_ADDR, &regAddr, 1, true);
  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	while (m_tx_xfer_done == false);
#endif	
	
	m_rx_xfer_done = false;	
  err_code = nrf_drv_twi_rx(&m_twi, MAX14676_DEV_ADDR, readValue, 2);
  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	while (m_rx_xfer_done == false)
	{
	}
#endif	
	
  return  readValue[0]; 
}

uint8_t max14676_Read01(uint8_t regAddr) 
{
  uint8_t readValue[2];

  nrf_drv_twi_txrx(&m_twi, MAX14676_DEV_ADDR, &regAddr, 1, readValue, 2);		
	
  return  readValue[0]; 
}

void max14676_WriteReg2(uint8_t regAddr,uint16_t regValue) 
{
	ret_code_t err_code;
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
  send_Buf[2] = (uint8_t)(regValue>>8);
    
	m_tx_xfer_done = false;
	err_code = nrf_drv_twi_tx(&m_twi, MAX14676_DEV_ADDR2, send_Buf, 3, false);
	APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	while (m_tx_xfer_done == false);
#endif	
}

uint16_t max14676_Read2(uint8_t regAddr) 
{
	ret_code_t err_code;
  uint16_t data16_1;
  uint8_t readValue[4];
	
   
	m_tx_xfer_done = false;
  err_code = nrf_drv_twi_tx(&m_twi, MAX14676_DEV_ADDR2, &regAddr, 1, true);
  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	while (m_tx_xfer_done == false);
#endif	
	
	m_rx_xfer_done = false;
	err_code = nrf_drv_twi_rx(&m_twi, MAX14676_DEV_ADDR2, readValue, 4);
  APP_ERROR_CHECK(err_code);
#if (TWI_BLOCKING_MODE == 0)	
	while (m_rx_xfer_done == false)
	{}
#endif	
	
	data16_1 = readValue[2];
	data16_1 = data16_1*256 +(uint16_t)readValue[1]; 
		
  return data16_1; 
}

uint32_t max14676_ReadReg2(uint8_t regAddr, uint16_t * pReadVal) 
{
	ret_code_t err_code;
  uint16_t data16_1;
  uint8_t readValue[4];
   
	m_tx_xfer_done = false;
  err_code = nrf_drv_twi_tx(&m_twi, MAX14676_DEV_ADDR2, &regAddr, 1, true);
	if(err_code != NRF_SUCCESS)
		return err_code;
#if (TWI_BLOCKING_MODE == 0)	
	while (m_tx_xfer_done == false);
#endif	
	
	m_rx_xfer_done = false;
	err_code = nrf_drv_twi_rx(&m_twi, MAX14676_DEV_ADDR2, readValue, 4);
	if(err_code != NRF_SUCCESS)
		return err_code;
#if (TWI_BLOCKING_MODE == 0)	
	while (m_rx_xfer_done == false);
#endif	
	
	data16_1 = readValue[2];
	data16_1 = data16_1*256 +(uint16_t)readValue[1]; 
	
  *pReadVal = data16_1;
  return err_code;
}

uint8_t status;
//uint32_t RCC_CSR_reg = 0;
void max14676_init(void) 
{
   //max14676_GetVer();
   //if(!max14676Exist) return;

   status = max14676_Read(MAX14676_INT_A);                       // Clear All interrupt flags
	 status = max14676_Read(MAX14676_INT_B);
 //status = max14676_Read(MAX14676_STATUS_A);
 //status = max14676_Read(MAX14676_STATUS_B);

   max14676_WriteReg(MAX14676_PWR_CFG_ADDR,  0x80);              // Power on
	
   max14676_WriteReg(MAX14676_CHGCNTLB,      0x10);              // 0x10 --> 7.5mA,  0x11 -- > 15mA
   max14676_WriteReg(MAX14676_ILIMCNTL_ADDR, 0x17);              // 00010111, 101->1000mA, 11 -> 
   max14676_WriteReg(MAX14676_CHGCNTLA,      0x1C);//0x18        // 0x0B  = 0x00010100 (0x14)   --->250mA
                                                                 //       = 0x00011100 (0x1C)   --->350mA	
   //max14676_WriteReg(MAX14676_LDOCFG,0x40);//0x18
   //data = max14676_ReadReg(0x0);
   //data = max14676_ReadReg(0x05);
   //max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x80); 
   //max14676_WriteReg(0x11, 0x84); 
   max14676_WriteReg(MAX14676_PCHGCNTL,      0x6e);              // 0 11 01 110, 11->40mA, 110 -> 2.90V     
	 
	 max14676_WriteReg(MAX14676_CDETCNTLB,     0x84);              // Charger automatically restarts when Vbat drops below     	 
	 max14676_WriteReg(MAX14676_CHGVSET,       0xC0);              // 220mV Recharge Threshold in Relation to VBAT
	 
  //testing
   //data =max14676_ReadReg2(0x02);  
   //data =max14676_ReadReg2(0x04);		

   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status | CHGSTAT_MASK);     // Enable ChgStat Interrupt
	 
   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     USBOK_MASK);                // Enable USBOK Interrupt
	 
	 status = max14676_Read(MAX14676_STATUS_A);
	 if(status == CHARGER_OFF)
	 {
		 //ChagerStatusFlag = 0;
	 }
	 else if(status == CHARGER_TIMER_DONE)
	 {
		 //ChagerStatusFlag = 1;
	 }
	 else
	 {
		 //ChagerStatusFlag = 2;
	 }	 
	 //status = max14676_Read(MAX14676_STATUS_C);
	 //max14676_WriteReg(0x08, 0x55);     // Enable ChgStat Interrupt
	 TestValue = max14676_Read(MAX14676_CHIP_ID);
}

void EnableCHG(void) 
{  
   //max14676_GetVer();
   //if(!max14676Exist) return;

	 status = max14676_Read(MAX14676_INT_A);
	 status = max14676_Read(MAX14676_INT_B);	
	 status = max14676_Read(MAX14676_STATUS_A);
   status = max14676_Read(MAX14676_STATUS_B);

	 status = max14676_Read(MAX14676_PWR_CFG_ADDR);
   max14676_WriteReg(MAX14676_PWR_CFG_ADDR,  0x80); 
	
	 status = max14676_Read(MAX14676_CHGCNTLB);
   max14676_WriteReg(MAX14676_CHGCNTLB,      0x10);       // Charged Done when charging current is 7.5mA             
	
	 status = max14676_Read(MAX14676_ILIMCNTL_ADDR);
   max14676_WriteReg(MAX14676_ILIMCNTL_ADDR, 0x17);              

	 status = max14676_Read(MAX14676_CHGCNTLA);
   max14676_WriteReg(MAX14676_CHGCNTLA,      0x1C);//0x18              
                                               
	 status = max14676_Read(MAX14676_PCHGCNTL);	
   max14676_WriteReg(MAX14676_PCHGCNTL,      0x6e);      

   max14676_WriteReg(MAX14676_CDETCNTLB,     0x84);              // Charger automatically restarts when Vbat drops below     	 	
	 max14676_WriteReg(MAX14676_CHGVSET,       0xC0);              // 220mV Recharge Threshold in Relation to VBAT


   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status | CHGSTAT_MASK);     // Enable ChgStat Interrupt

   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     status | USBOK_MASK);   
}

void DisableCHG(void) 
{  
   max14676_GetVer();
   if(!max14676Exist) return;

   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status & (~CHGSTAT_MASK));     // Disable ChgStat Interrupt

   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     status & (~USBOK_MASK));   
	 
	 status = max14676_Read(MAX14676_INT_A);                                  // Clear Interrupt flag
	 status = max14676_Read(MAX14676_INT_B);		                              // Clear Interrupt flag  
	 
	 max14676_WriteReg(MAX14676_CHGCNTLB,      0x00);                         // Charger disabled
}

void DisbaleInt(void) 
{  
   max14676_GetVer();
   if(!max14676Exist) return;
	
   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status & (~CHGSTAT_MASK));     // Enable ChgStat Interrupt

   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     status & (~USBOK_MASK));   
	 
	 status = max14676_Read(MAX14676_INT_A);
	 status = max14676_Read(MAX14676_INT_B);		 
}


//return the percentage 
uint8_t max14676_GetBatteryCharge(void) 
{
	uint16_t data=0; 
    uint32_t result;
    result =max14676_ReadReg2(0x04, &data); 
	if(result ==0)
		return (data/256);
	else 
		return 0;
	 
}
uint16_t max14676_GetBatteryChargeVoltage( void) 
{
	uint16_t data=0; 
  //(void)max14676_ReadReg2(0x02, &data); 
	data = max14676_Read2(0x02);   
	return data;
}

void max14676_GetVer(void) 
{
  uint8_t result;
  uint8_t readValue;  
  
  result =  max14676_ReadReg(MAX14676_CHIP_ID, &readValue);                      

  
  if((result == 0 )&&(readValue == 0x2E))
    max14676Exist = 2;  
  else 
  {
     max14676Exist = 0; 
  }
}
// assume max14676 exist

void max14676_process(void)
{
  uint8_t data;
//uint8_t status;
	uint8_t myCharge;
		
 	//DisableInterrupts; 
 	max14676Voltage = max14676_GetBatteryChargeVoltage(); 
	
 	myCharge = max14676_GetBatteryCharge();
		
	/*status =  */max14676_ReadReg(MAX14676_STATUS_A, &data);
 	//EnableInterrupts; 
  if(!max14676Charge)
  {
     max14676Charge += myCharge;
	   max14676Charge = max14676Charge>>1;
	}
  max14676Charge = myCharge;   
		
  max14676ChargeStat   = (/*data == 0x5 || */ data == CHARGER_TIMER_DONE) ? 1 : 0;   //  //1 charged; 0 uncharged
  if(max14676Voltage >= 9600)    //3V              
     max14676Voltage_old = max14676Voltage;    
  else
     max14676Voltage = max14676Voltage_old;      
  max14676Voltage = max14676Voltage/625 * 78;
}


void max14676_poweroff(void)
{
	//volatile uint16_t data=0; 	
	for(;;)
	{
		//DisbaleInt();
		//HAL_Delay(10);
		max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x5);
		//HAL_Delay(10);
	}
}



/** @} */

