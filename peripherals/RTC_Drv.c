/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#include <stdio.h>
#include "max14676.h"
#include "RTC_Drv.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "ble_lbs.h"
#include "ble_app.h"

#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28

//volatile byte I2C_transfer_complete;
uint8_t RTC_Buffer[MAX_RTC_LENGTH];

uint16_t return_result;

#define SETUP_TIMEx
#ifdef SETUP_TIME
  const unsigned char Build_date[] =__DATE__;
  const unsigned char Build_time[] =__TIME__;
  
  const char* month_map[12] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec", 
  };

  byte strtoi_2 (char *str)
  {
  	unsigned char rvalue;
    char *c;

  	rvalue = 0;
  	/* Check for invalid chars in str */
  	for ( c = str; *c != '\0'; ++c)
  	{
  	  byte val=(byte)(*c-'0');
      rvalue = (rvalue * 10) + val;
  	}
    return rvalue;
  }
  
  byte strmon_to_i(const char* month) 
  {
     byte i,j;
     const char* temp_char = month;
         
     for(i = 0;i<12;i++) 
     {
       for(j=0;j<3;j++) 
       {
         if(month_map[i][j] != *temp_char++) 
         {
           temp_char = month;
           break;
         }
       }
       if(j == 3)
        break;
     }
     
     return i+1;
  }
#endif


uint8_t Register_Read(uint8_t RTC_Register_Address)
{
  ret_code_t err_code;
  uint8_t readValue[2];
   
  //err_code = nrf_drv_twi_tx(&m_twi, VCNL3020_DEV_ADDR, &regAddr, 1, true);
  //APP_ERROR_CHECK(err_code);
	
  err_code = nrf_drv_twi_rx(&m_twi, RTC_Register_Address, readValue, 2);
  APP_ERROR_CHECK(err_code);
	
  return  readValue[0];   
}

void Register_Write(uint8_t RTC_Register_Address, uint8_t register_value) 
{
  
  ret_code_t err_code;   
  RTC_Buffer[0] = register_value;
	
  err_code = nrf_drv_twi_tx(&m_twi, RTC_Register_Address, RTC_Buffer, 1, false);			
  APP_ERROR_CHECK(err_code);
	
}


void rtc_int2_event_handler(uint8_t pin_no, uint8_t int2_action)
{
//    ret_code_t err_code;
//	  uint8_t send_buf[5];

    switch (pin_no)
    {
        case RTC_INT2:
				    if(!int2_action)
						{
							//BRIGHT_LED_ON();
						}
						else
						{
							//BRIGHT_LED_OFF();
						}
            break;
					
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

/*
** ===================================================================
**     void     :  Initialize_RTC(void)
** ===================================================================
*/   
#define RTC_STATUS_INIT_VALUE  0x40 // 24 Hour mode
uint8_t Initialize_RTC(void)
{
  
//  uint8_t cs1,cs2; 
  nrf_delay_us(500);
  /*cs1 = */(void)Register_Read(RTC_CTL_ST1);
  /*cs2 = */(void)Register_Read(RTC_CTL_ST2);
  //if((cs1 & RTC_POC )||(cs2 &0x8))
  {
     //The time get lost  warning!!! need connect to pc 
     Register_Write(RTC_CTL_ST1,    RTC_STATUS_INIT_VALUE);
     Register_Write(RTC_CTL_ST2,    0x08);                    //enable output of user set frequency, INT2FE = 1, INT2ME = 0, INT2AE = 0
     Register_Write(RTC_INT2_ALARM, 0x80);                    //output 1Hz
  }
  //Register_Write(RTC_CLOCK_CORRECTION, 0x02); 
  return 0;
}  


uint8_t Get_HourMinuteSecond(uint8_t *time_tt) 
{  
	ret_code_t err_code;
	uint8_t i,PM_AM_flag;
  err_code = nrf_drv_twi_rx(&m_twi, RTC_REAL_DATA2, time_tt, 3);
	APP_ERROR_CHECK(err_code);
  for (i = 0; i < 3; ++i) {
       
    if(i==0) 
    {     
      PM_AM_flag = time_tt[i] & 0x02;  
      time_tt[i] = Flip_Bits(time_tt[i] & 0xFC);
    }
    else
      time_tt[i] = Flip_Bits(time_tt[i]);    
  }  
  return PM_AM_flag;
}


#if RTC_TIME_FIFTEEN_MINUTE
  #define  SLEEP_TIME_PERIOD1    15
  #define  SLEEP_TIME_PERIOD2    5
#else
  #define  SLEEP_TIME_PERIOD1    1
  #define  SLEEP_TIME_PERIOD2    1
#endif


void RTC_Init(uint8_t mode)
{
  ret_code_t err_code;
  uint8_t cur_time[3];
//  uint8_t cs1,cs2; 
  nrf_delay_us(100);
  
  //DisableInterrupts; 
  //Register_Write(RTC_CTL_ST1, 80); 
  /*cs1 = */Register_Read(RTC_CTL_ST1);                       // Clear interupt flag          
  nrf_delay_us(1000);
  Register_Write(RTC_CTL_ST2,    0x00);                       //Clear Interrupt pin (make it high) 
  nrf_delay_us(1000);
  if(mode == 0)
  {
     //The time get lost  warning!!! need connect to pc 
     //Register_Write(RTC_CTL_ST2,    0x08);                    //enable output of user set frequency, INT2FE = 1, INT2ME = 0, INT2AE = 0
     //Cpu_Delay100US(10); 
     //Register_Write(RTC_INT2_ALARM, 0x80);                    //output 1Hz
     //Cpu_Delay100US(10); 
  } 
  else
  {
     
    (void)Get_HourMinuteSecond(cur_time);
    
    cur_time[1] = BCD2BIN_t(cur_time[1]);
    RTC_Buffer[0] = 0x00;
    RTC_Buffer[1] = 0x00;
    
    
    if(mode == 1) 
    {      
       Register_Write(RTC_CTL_ST2,    0x02);                    // enable output of Alram interrupt, INT2FE = 0, INT2ME = 0, INT2AE = 1
       cur_time[1] +=1;                                         // one interrupt each 10 minutes
       if(cur_time[1] >= 60)
         cur_time[1]=0;
               
     
       RTC_Buffer[2] = bin2bcd(cur_time[1]);
       RTC_Buffer[2] = Flip_Bits(RTC_Buffer[2]);       
       RTC_Buffer[2] = RTC_Buffer[2]|0x01;
                 
       err_code = nrf_drv_twi_tx(&m_twi, RTC_INT2_ALARM, RTC_Buffer, 3, false);			
       APP_ERROR_CHECK(err_code);       
    }      
    else if(mode == 2)  // power off  mode , one interrupt every 1 minute
    {
       Register_Write(RTC_CTL_ST2,    0x02);                    // enable output of Alram interrupt, INT2FE = 0, INT2ME = 0, INT2AE = 1
         cur_time[1] +=SLEEP_TIME_PERIOD1;                                
         if(cur_time[1] >= 60)
           cur_time[1] = cur_time[1]-60;

     RTC_Buffer[2] = bin2bcd(cur_time[1]);
     RTC_Buffer[2] = Flip_Bits(RTC_Buffer[2]);       
     RTC_Buffer[2] = RTC_Buffer[2]|0x01;
            
     err_code = nrf_drv_twi_tx(&m_twi, RTC_INT2_ALARM, RTC_Buffer, 3, false);			
     APP_ERROR_CHECK(err_code);				 
    }
    else if(mode == 3) 
    {
   #if 0
       if(gftInfo.batState == BAT_STAT_LOW)
       {
          cur_time[1] =0;                                                 
       } 
       else
       {
         Register_Write(RTC_CTL_ST2,    0x02);                    // enable output of Alram interrupt, INT2FE = 0, INT2ME = 0, INT2AE = 1
         cur_time[1] +=1;                                         // one interrupt each 1 minutes
         if(cur_time[1] >= 60) 
           cur_time[1] = 0;
       } 
       
       if(gftInfo.batState != BAT_STAT_LOW) 
       {               
         RTC_Buffer[2] = bin2bcd(cur_time[1]);
         RTC_Buffer[2] = Flip_Bits(RTC_Buffer[2]);       
         RTC_Buffer[2] = RTC_Buffer[2]|0x01;
            
         err_code = nrf_drv_twi_tx(&m_twi, RTC_INT2_ALARM, RTC_Buffer, 3, false);			
         APP_ERROR_CHECK(err_code);				 
       }
		#endif
    }        
    ////////////// Added by Jason Chen, 2017.04.11 ////////////////////////////////////////////////////////////
    else if(mode == 4)                                          // power off  mode , one interrupt every 1 minute, but different second.
    {
       Register_Write(RTC_CTL_ST2,    0x02);                    // enable output of Alram interrupt, INT2FE = 0, INT2ME = 0, INT2AE = 1
       //if(myId > 0)
       //  cur_time[1] += 5 + myId/12;
       //else
         cur_time[1] += 5;
       if(cur_time[1] >= 60)
         cur_time[1] = cur_time[1]-60;

       RTC_Buffer[2] = bin2bcd(cur_time[1]);
       RTC_Buffer[2] = Flip_Bits(RTC_Buffer[2]);       
       RTC_Buffer[2] = RTC_Buffer[2]|0x01;
            
       err_code = nrf_drv_twi_tx(&m_twi, RTC_INT2_ALARM, RTC_Buffer, 3, false);			
       APP_ERROR_CHECK(err_code);
    }
    /////////////// Ended by Jason Chen, 2017.04.11 //////////////////////////
    
    
  }
  //EnableInterrupts;
}  


uint8_t Initialize_RTC2(void)
{
  //bool rtc_loop=1;
  uint8_t tmp; 
  
  nrf_delay_us(200);
  
  tmp = Register_Read(RTC_CTL_ST2);
  if(tmp)
  {
  	Register_Write(RTC_CTL_ST1, RTC_STATUS_INIT_VALUE);
    Register_Write(RTC_CTL_ST2, 0);
  }
  return 0;
}  

uint8_t rtcGetRegisterValue(uint8_t registerAddr)
{
	return Register_Read(registerAddr);

}

uint8_t Get_YearMonthDay(uint8_t *date_time) 
{
	ret_code_t err_code;
  uint8_t i,PM_AM_flag;
  //DisableInterrupts;   
  err_code = nrf_drv_twi_rx(&m_twi, RTC_REAL_DATA1, date_time, 7);
  APP_ERROR_CHECK(err_code);
		
  //EnableInterrupts;
  //while(!(I2C_transfer_complete & RECV_COMPLETE));
  //I2C_transfer_complete &= ~RECV_COMPLETE;  
  for (i = 0; i < 7; ++i) {
       
    if(i==4) 
    {     
      PM_AM_flag = date_time[i] & 0x02;  
      date_time[i] = Flip_Bits(date_time[i] & 0xFC);
    }
    else
      date_time[i] = Flip_Bits(date_time[i]);    
  }  
  return PM_AM_flag;
}
//void Get_TimeDifference(byte *dataStr1, byte *dataStr2)
//{
//}

void Set_YearMonthDay(uint8_t *date_str) 
{
	ret_code_t err_code;
  uint8_t i;
  
  //I2C_transfer_complete = 0x00;
  for(i=0;i<7;i++) 
  {
    // Set real-time data 1
    RTC_Buffer[i] = *date_str++; 
    RTC_Buffer[i] = bin2bcd(RTC_Buffer[i]); 
    RTC_Buffer[i] = Flip_Bits(RTC_Buffer[i]);       
  }
  //DisableInterrupts;  
   err_code = nrf_drv_twi_tx(&m_twi, RTC_REAL_DATA1, RTC_Buffer, 7, false);			
   APP_ERROR_CHECK(err_code);	  
  //EnableInterrupts; 
  //while(!(I2C_transfer_complete & SEND_COMPLETE));
  //I2C_transfer_complete &= ~SEND_COMPLETE; 
}


void Change_12or24(bool hour12or24) 
{
  uint8_t tmp;
  if(hour12or24)
    tmp = 0x70;     // Write 24hours Setup
  else
    tmp = 0x00;     // Write 12hours Setup  
  Register_Write(RTC_CTL_ST1, tmp);          
}

/*
** ===================================================================
**     byte     :  bin2bcd (byte x)
** ===================================================================
*/  
uint8_t bin2bcd (uint8_t x)
{
	return (x%10) | ((x/10) << 4);
}


/*
** ===================================================================
**     char     :  Flip_Bits(char x)
** ===================================================================
*/   
char Flip_Bits(char x)
{
	x = ((x >> 1) & 0x55) | ((x << 1) & 0xaa);
	x = ((x >> 2) & 0x33) | ((x << 2) & 0xcc);
	return (x >> 4) | (x << 4);
}    



