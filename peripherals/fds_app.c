/**
 * Copyright (c) 2015 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Blinky Sample Application main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */

#include <stdint.h>
#include <string.h>

#include "fds.h"
#include "ble_app.h"
#include "fds_app.h"

#include "peer_data_storage.h"
#include "ble_gap.h"

#define NRF_LOG_MODULE_NAME fds_app_c
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_strerror.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();

#if 0
/* Array to map FDS return values to strings. */
static char const * fds_err_str[] =
{
    "FDS_SUCCESS",
    "FDS_ERR_OPERATION_TIMEOUT",
    "FDS_ERR_NOT_INITIALIZED",
    "FDS_ERR_UNALIGNED_ADDR",
    "FDS_ERR_INVALID_ARG",
    "FDS_ERR_NULL_ARG",
    "FDS_ERR_NO_OPEN_RECORDS",
    "FDS_ERR_NO_SPACE_IN_FLASH",
    "FDS_ERR_NO_SPACE_IN_QUEUES",
    "FDS_ERR_RECORD_TOO_LARGE",
    "FDS_ERR_NOT_FOUND",
    "FDS_ERR_NO_PAGES",
    "FDS_ERR_USER_LIMIT_REACHED",
    "FDS_ERR_CRC_CHECK_FAILED",
    "FDS_ERR_BUSY",
    "FDS_ERR_INTERNAL",
};

/* Array to map FDS events to strings. */
static char const * fds_evt_str[] =
{
    "FDS_EVT_INIT",
    "FDS_EVT_WRITE",
    "FDS_EVT_UPDATE",
    "FDS_EVT_DEL_RECORD",
    "FDS_EVT_DEL_FILE",
    "FDS_EVT_GC",
};
#endif
#define PEER_MANAGER_FILE_ID            PEER_ID_TO_FILE_ID               //( PDS_FIRST_RESERVED_FILE_ID)    //!< Macro for converting a @ref pm_peer_id_t to an FDS file ID.
//#define FILE_ID_TO_PEER_ID            (-PDS_FIRST_RESERVED_FILE_ID)    //!< Macro for converting an FDS file ID to a @ref pm_peer_id_t.
//#define DATA_ID_TO_RECORD_KEY         ( PDS_FIRST_RESERVED_RECORD_KEY) //!< Macro for converting a @ref pm_peer_data_id_t to an FDS record ID.
//#define RECORD_KEY_TO_DATA_ID         (-PDS_FIRST_RESERVED_RECORD_KEY) //!< Macro for converting an FDS record ID to a @ref pm_peer_data_id_t.

#define PEER_RANK_DATA_KEY_ID               (DATA_ID_TO_RECORD_KEY + PM_PEER_DATA_ID_PEER_RANK)
#define PEER_BONDING_DATA_KEY_ID            (DATA_ID_TO_RECORD_KEY + PM_PEER_DATA_ID_BONDING)
#define PEER_GATT_LOCAL_DATA_KEY_ID         (DATA_ID_TO_RECORD_KEY + PM_PEER_DATA_ID_GATT_LOCAL)
#define PEER_CENTRAL_ADDR_RES_DATA_KEY_ID   (DATA_ID_TO_RECORD_KEY + PM_PEER_DATA_ID_CENTRAL_ADDR_RES)

#if 0
static char const * fds_record_key_str[] =
{
	  "PM_PEER_DATA_ID_FIRST",                             //= PM_PEER_DATA_ID_FIRST_VX,                   /**< @brief The smallest data ID. */
    "PM_PEER_DATA_ID_SERVICE_CHANGED_PENDING",           //= PM_PEER_DATA_ID_SERVICE_CHANGED_PENDING_V1, /**< @brief The data ID for service changed state. Type: bool. */	  
    "PM_PEER_DATA_ID_GATT_LOCAL_V1_ME",                  //= PM_PEER_DATA_ID_GATT_LOCAL_V1,              /**< @brief The data ID of the first version of local GATT data. */
		"PM_PEER_DATA_ID_GATT_REMOTE_V1_ME",                 //= PM_PEER_DATA_ID_GATT_REMOTE_V1,             /**< @brief The data ID of the first version of remote GATT data. */
    "PM_PEER_DATA_ID_APPLICATION",                       //= PM_PEER_DATA_ID_APPLICATION_V1,             /**< @brief The data ID for application data. Type: uint8_t array. */	
    "PM_PEER_DATA_ID_GATT_REMOTE",                       //= PM_PEER_DATA_ID_GATT_REMOTE_V2,             /**< @brief The data ID for remote GATT data. Type: uint8_t array. */	
	  "PM_PEER_DATA_ID_PEER_RANK",                         //= PM_PEER_DATA_ID_PEER_RANK_V1,               /**< @brief The data ID for peer rank. See @ref pm_peer_rank_highest. Type: uint32_t. */
    "PM_PEER_DATA_ID_BONDING",                           //= PM_PEER_DATA_ID_BONDING_V2,                 /**< @brief The data ID for bonding data. Type: @ref pm_peer_data_bonding_t. */	
    "PM_PEER_DATA_ID_GATT_LOCAL",                        //= PM_PEER_DATA_ID_GATT_LOCAL_V2,              /**< @brief The data ID for local GATT data (sys attributes). Type: @ref pm_peer_data_local_gatt_db_t. */	
    "PM_PEER_DATA_ID_CENTRAL_ADDR_RES",                  //= PM_PEER_DATA_ID_CENTRAL_ADDR_RES_V1,        /**< @brief The data ID for central address resolution. See @ref pm_peer_id_list. Type: uint32_t. */
    "PM_PEER_DATA_ID_LAST",                              //= PM_PEER_DATA_ID_LAST_VX,                    /**< @brief One more than the highest data ID. */	
    "PM_PEER_DATA_ID_INVALID",                           //= PM_PEER_DATA_ID_INVALID_VX,                 /**< @brief A data ID guaranteed to be invalid. */
};
#endif

extern void Buzzer_Beep(void);
/* Dummy configuration data. */
configuration_t gft4_cfg =
{
	  .timer_enable        = false,
		.erase_bonds         = false,	
    .device_name         = "dummy",	
    .phy_select          = 2,			
		.phy_config.tx_phys  = BLE_GAP_PHY_CODED,
		.phy_config.rx_phys  = BLE_GAP_PHY_CODED,
		.mode                = WORKING_MODE,
};

//static fds_record_desc_t description_1 = {0};
//static fds_find_token_t  token_1  = {0};

/* A record containing dummy configuration data. */
fds_record_t const gft4_config_record =
{
    .file_id           = CONFIG_FILE,
    .key               = CONFIG_REC_KEY,
    .data.p_data       = &gft4_cfg,
    /* The length of a record is always expressed in 4-byte units (words). */
    .data.length_words = (sizeof(gft4_cfg) + 3) / sizeof(uint32_t),
};

/* Keep track of the progress of a delete_all operation. */
static struct
{
    bool delete_next;   //!< Delete next record.
    bool pending;       //!< Waiting for an fds FDS_EVT_DEL_RECORD event, to delete the next record.
} m_delete_all;

/* Keep track of the progress of a delete Bonded operation. */
static struct
{
    bool     delete_next;   //!< Delete next record.
    uint8_t  bonded_sel;
    bool     pending;       //!< Waiting for an fds FDS_EVT_DEL_RECORD event, to delete the next record.
} m_delete_bonded;

static struct
{
    bool update;        //!< update record.
    bool pending;       //!< Waiting for an fds FDS_EVT_DEL_RECORD event, to delete the next record.
} m_update_fds;

/* Flag to check fds initialization. */
static bool volatile m_fds_initialized = false;

static void fds_evt_handler_local(fds_evt_t const * p_evt)
{
    //NRF_LOG_GREEN("Event: %s received (%s)"DEFAULT_COLOR,
     //             fds_evt_str[p_evt->id],
     //             fds_err_str[p_evt->result]);
	
    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
							  NRF_LOG_BLUE("FDS_EVT_INIT ==> OK"DEFAULT_COLOR);
            }
						else
							NRF_LOG_BLUE("FDS_EVT_INIT ==> FAIL"DEFAULT_COLOR);
            break;

        case FDS_EVT_WRITE:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
							  NRF_LOG_RED("FDS_EVT_WRITE ==>"DEFAULT_COLOR);
                NRF_LOG_BLUE("Record ID:\t\t0x%04x",               p_evt->write.record_id);
                NRF_LOG_BLUE("File ID:\t\t0x%04x",                 p_evt->write.file_id);
                NRF_LOG_BLUE("Record key:\t\t0x%04x"DEFAULT_COLOR, p_evt->write.record_key);
							  
							  if( p_evt->write.file_id == PEER_MANAGER_FILE_ID)
								{
									//NRF_LOG_BLUE("Bonding info Write: \t%s"DEFAULT_COLOR,fds_record_key_str[p_evt->write.record_key + FILE_ID_TO_PEER_ID]);
									if(p_evt->write.record_key == PEER_BONDING_DATA_KEY_ID)
									  get_peer_ble_addr(__LINE__);
								}
							
            }
        } break;

        case FDS_EVT_DEL_RECORD:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
							  NRF_LOG_RED("FDS_EVT_DEL ==>"DEFAULT_COLOR);
                NRF_LOG_BLUE("Record ID:\t\t\t0x%04x",               p_evt->del.record_id);
                NRF_LOG_BLUE("File ID:\t\t\t0x%04x",                 p_evt->del.file_id);
                NRF_LOG_BLUE("Record key:\t\t\t0x%04x"DEFAULT_COLOR, p_evt->del.record_key);							
							  if( p_evt->write.file_id == PEER_MANAGER_FILE_ID)
								{
									//NRF_LOG_BLUE("DEL Record: \t\t%s"DEFAULT_COLOR,fds_record_key_str[p_evt->write.record_key + FILE_ID_TO_PEER_ID]);
								}							
            }
            m_delete_all.pending = false;
        } break;
				case FDS_EVT_UPDATE:                                                //!< Event for @ref fds_record_update.
				{
					  if (p_evt->result == FDS_SUCCESS)
					  {
						    NRF_LOG_RED("FDS_EVT_UPDATE ==>"DEFAULT_COLOR);
                NRF_LOG_BLUE("Record ID:\t\t0x%04x",               p_evt->write.record_id);
                NRF_LOG_BLUE("File ID:\t\t0x%04x",                 p_evt->write.file_id);
                NRF_LOG_BLUE("Record key:\t\t0x%04x"DEFAULT_COLOR, p_evt->write.record_key);						

  							if( p_evt->write.file_id == PEER_MANAGER_FILE_ID)
								{
									//NRF_LOG_BLUE("Bonding info Update: \t%s "DEFAULT_COLOR,fds_record_key_str[p_evt->write.record_key + FILE_ID_TO_PEER_ID]);
								}								
					  }
				}break;
				case FDS_EVT_DEL_FILE:                                              //!< Event for @ref fds_file_delete.
				{
					  if (p_evt->result == FDS_SUCCESS)
					  {
						    NRF_LOG_BLUE("FDS_EVT_DEL_FILE ==>"DEFAULT_COLOR);
                NRF_LOG_BLUE("Record ID:\t\t\t0x%04x",               p_evt->del.record_id);
                NRF_LOG_BLUE("File ID:\t\t\t0x%04x",                 p_evt->del.file_id);
                NRF_LOG_BLUE("Record key:\t\t\t0x%04x"DEFAULT_COLOR, p_evt->del.record_key);													
							  if( p_evt->write.file_id == PEER_MANAGER_FILE_ID)
								{
									//NRF_LOG_BLUE("DEL File: \t%s"DEFAULT_COLOR,fds_record_key_str[p_evt->write.record_key + FILE_ID_TO_PEER_ID]);
								}														
					  }					
				}break;
				case FDS_EVT_GC:                                                    //!< Event for @ref fds_gc.
				{
					  if (p_evt->result == FDS_SUCCESS)
					  {
						    NRF_LOG_BLUE("FDS_EVT_GC ==>"DEFAULT_COLOR);
					  }					
				}break;
        default:
            break;
    }
}

/**@brief   Begin deleting all records, one by one. */
void delete_all_begin(void)
{
    m_delete_all.delete_next = true;
}

static bool record_delete_next(void)
{
    fds_find_token_t  tok1   = {0};
    fds_record_desc_t desc1  = {0};

    if (fds_record_iterate(&desc1, &tok1) == FDS_SUCCESS)
    {
        ret_code_t rc = fds_record_delete(&desc1);
        if (rc != FDS_SUCCESS)
        {
            return false;
        }

        return true;
    }
    else
    {
        /* No records left to delete. */
        return false;
    }
}

static bool bonded_record_delete_next(void)
{
	  ret_code_t rc;
    fds_find_token_t  tok1   = {0};
    fds_record_desc_t desc1  = {0};

    if (fds_record_iterate(&desc1, &tok1) == FDS_SUCCESS)
    {			
			  //rc = fds_record_find(PEER_MANAGER_FILE_ID, PEER_BONDING_DATA_KEY_ID,          &desc1, &tok1);
			  //rc = fds_record_find(PEER_MANAGER_FILE_ID, PEER_RANK_DATA_KEY_ID,             &desc1, &tok1);
			  //rc = fds_record_find(PEER_MANAGER_FILE_ID, PEER_GATT_LOCAL_DATA_KEY_ID,       &desc1, &tok1);
			  //rc = fds_record_find(PEER_MANAGER_FILE_ID, PEER_CENTRAL_ADDR_RES_DATA_KEY_ID, &desc1, &tok1);			
        //rc = fds_record_find_in_file(PEER_MANAGER_FILE_ID, &desc1, &tok1);
			  //if(rc != FDS_SUCCESS)
				//	return false;
			  //if(true)//rc == FDS_SUCCESS)
				{
					if(m_delete_bonded.bonded_sel == 0)
					{
						NRF_LOG_RED("fds_record_deleting(), Line: %d, %d"DEFAULT_COLOR, __LINE__, m_delete_bonded.bonded_sel);
						rc = fds_record_find_by_key(PEER_BONDING_DATA_KEY_ID, &desc1, &tok1);
						//rc = fds_record_find(PEER_MANAGER_FILE_ID, PEER_BONDING_DATA_KEY_ID, &desc1, &tok1);
						m_delete_bonded.bonded_sel = 1;
					}
					else if(m_delete_bonded.bonded_sel == 1)
					{
						NRF_LOG_RED("fds_record_deleting(), Line: %d, %d"DEFAULT_COLOR, __LINE__, m_delete_bonded.bonded_sel);
						rc = fds_record_find_by_key(PEER_CENTRAL_ADDR_RES_DATA_KEY_ID, &desc1, &tok1);
						m_delete_bonded.bonded_sel = 2;
					}
					else if(m_delete_bonded.bonded_sel == 2)
					{
						NRF_LOG_RED("fds_record_deleting(), Line: %d, %d"DEFAULT_COLOR, __LINE__, m_delete_bonded.bonded_sel);
						rc = fds_record_find_by_key(PEER_GATT_LOCAL_DATA_KEY_ID, &desc1, &tok1);
						m_delete_bonded.bonded_sel = 3;
					}
					else if(m_delete_bonded.bonded_sel == 3)
					{
						NRF_LOG_RED("fds_record_deleting(), Line: %d, %d"DEFAULT_COLOR, __LINE__, m_delete_bonded.bonded_sel);
						rc = fds_record_find_by_key(PEER_RANK_DATA_KEY_ID, &desc1, &tok1);
						m_delete_bonded.bonded_sel = 0xFF;
					}
          else
					{
						return false;
					}						
					if(rc == FDS_SUCCESS)
					{			
						ret_code_t rc = fds_record_delete(&desc1);
						
						if (rc != FDS_SUCCESS)
						{
								NRF_LOG_RED("fds_record_delete(), Error, Line: %d"DEFAULT_COLOR, m_delete_bonded.bonded_sel - 1);//__LINE__);
								return false;
						}
						NRF_LOG_RED("fds_record_delete(), OK, Line: %d"DEFAULT_COLOR, m_delete_bonded.bonded_sel - 1);//__LINE__);
						return true;
					}
					else
					{
					  if(m_delete_bonded.bonded_sel != 0xFF)
						  return true;
					  return false;						
					}						
			  }
				//else
				//{
				//	if(m_delete_bonded.bonded_sel != 0xFF)
				//		return true;
				//	return false;
				//}
    }
    else
    {
        /* No records left to delete. */
        return false;
    }
}

void update_fds_begin(void)
{
    m_update_fds.update  = true;
	  m_update_fds.pending = true;
}

void update_fds_pending_clear(void)
{
	  m_update_fds.pending = false;
}

void Bonded_delete_begin(void)
{
    m_delete_bonded.delete_next  = true;
	  m_delete_bonded.bonded_sel   = 0;
	  m_delete_bonded.pending = true;
}

#if 0
/**@brief   Process a delete all command.
 *
 * Delete records, one by one, until no records are left.
 */
void delete_all_process(void)
{
    if (   m_delete_all.delete_next
        & !m_delete_all.pending)
    {
        NRF_LOG_INFO("Deleting next record.");

        m_delete_all.delete_next = record_delete_next();
        if (!m_delete_all.delete_next)
        {
            NRF_LOG_CYAN("No records left to delete.");
        }
    }
}
#endif

void fds_process(void)
{
    if (   m_delete_all.delete_next
        & !m_delete_all.pending)
    {
        NRF_LOG_INFO("Deleting next record.");

        m_delete_all.delete_next = record_delete_next();
			 			
        if (!m_delete_all.delete_next)
        {
            NRF_LOG_CYAN("No records left to delete.");
					  (void)fds_gc();
        }
    }
		
    if (m_delete_bonded.delete_next)
    {
        NRF_LOG_INFO("Deleting next bonded record.");

        m_delete_bonded.delete_next = bonded_record_delete_next();
			 			
        if (!m_delete_bonded.delete_next)
        {
            NRF_LOG_CYAN("No bonded records left to delete.");
					  (void)fds_gc();
        }
    }
		
    if ( m_update_fds.update)// & !m_update_fds.pending)
    {
        NRF_LOG_INFO("Update FDS Record.");			
			  gft4_config_update();
			  (void)fds_gc();	
			
				m_update_fds.update = false;
    }
}

/**@brief   Sleep until an event is received. */
void fds_manage(void)
{
    (void) sd_app_evt_wait();
}

/**@brief   Wait for fds to initialize. */
static void wait_for_fds_ready(void)
{
    while (!m_fds_initialized)
    {
        fds_manage();
    }
}

void display_fds_status(void)
{
	ret_code_t rc;
	
  fds_stat_t stat = {0};
  rc = fds_stat(&stat);
  APP_ERROR_CHECK(rc);	

  NRF_LOG_RED("Found %d open records."DEFAULT_COLOR,                                  stat.open_records);				
  NRF_LOG_RED("Found %d available pages."DEFAULT_COLOR,                               stat.pages_available);		
  NRF_LOG_RED("Found %d valid records."DEFAULT_COLOR,                                 stat.valid_records);
  NRF_LOG_RED("Found %d dirty records (ready to be garbage collected)."DEFAULT_COLOR, stat.dirty_records);
	NRF_LOG_RED("Found %d words reserved."DEFAULT_COLOR,                                stat.words_reserved);
	NRF_LOG_RED("Found %d words used."DEFAULT_COLOR,                                    stat.words_used);
	NRF_LOG_RED("Found %d largest contig."DEFAULT_COLOR,                                stat.largest_contig);
	NRF_LOG_RED("Found %d freeable words."DEFAULT_COLOR,                                stat.freeable_words);
	NRF_LOG_RED("Found corruption: %s."DEFAULT_COLOR,                                   stat.corruption?"true":"false");	
}

void fds_app_init(void)
{
	  ret_code_t rc;
    /* Register first to receive an event when initialization is complete. */
    (void) fds_register(fds_evt_handler_local);
    rc = fds_init();
    APP_ERROR_CHECK(rc);	

    /* Wait for fds to initialize. */
    wait_for_fds_ready();

    display_fds_status();
		
    fds_record_desc_t desc = {0};
    fds_find_token_t  tok  = {0};

    rc = fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &tok);		

    if (rc == FDS_SUCCESS)
    {
        /* A config file is in flash. Let's update it. */
        fds_flash_record_t config = {0};

        /* Open the record and read its contents. */
        rc = fds_record_open(&desc, &config);
        APP_ERROR_CHECK(rc);
				
				NRF_LOG_YELLOW("file_id:      0x%04x"DEFAULT_COLOR, config.p_header->file_id);
				NRF_LOG_YELLOW("record_key:   0x%04x"DEFAULT_COLOR, config.p_header->record_key);					 
				NRF_LOG_YELLOW("record_id:    0x%04x"DEFAULT_COLOR, config.p_header->record_id);
				NRF_LOG_YELLOW("length_words: %d"DEFAULT_COLOR,     config.p_header->length_words);	
				NRF_LOG_YELLOW("crc16:        0x%04x"DEFAULT_COLOR, config.p_header->crc16);					
				
        /* Copy the configuration from flash into gft4_cfg. */
        memcpy(&gft4_cfg, config.p_data, sizeof(configuration_t));
				
				NRF_LOG_RED("FDS Device Name: %s, Line:%d"DEFAULT_COLOR, gft4_cfg.device_name, __LINE__);
				NRF_LOG_RED("FDS PHY config: %d, %s, Line:%d"DEFAULT_COLOR, gft4_cfg.phy_config.tx_phys, phy_str(gft4_cfg.phy_config), __LINE__);			
					
        if((gft4_cfg.phy_config.tx_phys > (BLE_GAP_PHY_1MBPS|BLE_GAP_PHY_2MBPS|BLE_GAP_PHY_CODED))
				 ||(gft4_cfg.phy_config.rx_phys > (BLE_GAP_PHY_1MBPS|BLE_GAP_PHY_2MBPS|BLE_GAP_PHY_CODED)))
				{
					m_test_params.phy_select        = gft4_cfg.phy_select          = 2;
					m_test_params.phys.tx_phys      = gft4_cfg.phy_config.tx_phys  = BLE_GAP_PHY_CODED;
					m_test_params.phys.rx_phys      = gft4_cfg.phy_config.rx_phys  = BLE_GAP_PHY_CODED;
				}
				else if((gft4_cfg.phy_config.tx_phys <= (BLE_GAP_PHY_1MBPS|BLE_GAP_PHY_2MBPS|BLE_GAP_PHY_CODED))
				      &&(gft4_cfg.phy_config.rx_phys <= (BLE_GAP_PHY_1MBPS|BLE_GAP_PHY_2MBPS|BLE_GAP_PHY_CODED)))
				{
					m_test_params.phy_select        = gft4_cfg.phy_select;
					m_test_params.phys.tx_phys      = gft4_cfg.phy_config.tx_phys;
					m_test_params.phys.rx_phys      = gft4_cfg.phy_config.rx_phys;
				}
				m_test_params.timer_enable        = gft4_cfg.timer_enable;
				m_test_params.erase_bonds         = gft4_cfg.erase_bonds;
				
				if((gft4_cfg.mode != WORKING_MODE) && (gft4_cfg.mode != BONDING_MODE))
				{
					m_test_params.mode = gft4_cfg.mode = WORKING_MODE;
					update_fds_begin();
				}
				else
				{
					m_test_params.mode = gft4_cfg.mode;
				}

				//(void)sd_ble_gap_phy_update(m_conn_handle, &m_test_params.phys);
				//NRF_LOG_YELLOW("Config file found, Sending request of updating PHY config to %s"DEFAULT_COLOR, phy_str(m_test_params.phys));
				
        /* Update boot count. */
        //gft4_cfg.boot_count++;				

        /* Close the record when done reading. */
        rc = fds_record_close(&desc);
        APP_ERROR_CHECK(rc);

        /* Write the updated record to flash. */
        //rc = fds_record_update(&desc, &gft4_config_record);
        //APP_ERROR_CHECK(rc);
				//NRF_LOG_YELLOW("Config file found, updating boot count to %d."DEFAULT_COLOR, gft4_cfg.boot_count);				
				
    }
    else
    {	  
        /* System config not found; write a new one. */
        NRF_LOG_RED("Creating the GFT4 config file..."DEFAULT_COLOR); 			
			  memcpy(gft4_cfg.device_name, device_full_name, sizeof(device_full_name));
        rc = fds_record_write(&desc, &gft4_config_record);
        APP_ERROR_CHECK(rc);
    }    
}				
		
void gft4_config_update(void)
{
	  ret_code_t rc;
			
    fds_record_desc_t description = {0};
    fds_find_token_t  token       = {0};

    rc = fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &description, &token);		
		
    if (rc == FDS_SUCCESS)
    {
        /* A config file is in flash. Let's update it. */
        fds_flash_record_t config = {0};

        /* Open the record and read its contents. */
        rc = fds_record_open(&description, &config);
        APP_ERROR_CHECK(rc);
				
				//NRF_LOG_YELLOW("fds_update() file_id:      0x%04x"DEFAULT_COLOR, config.p_header->file_id);
				//NRF_LOG_YELLOW("fds_update() record_key:   0x%04x"DEFAULT_COLOR, config.p_header->record_key);					 
				//NRF_LOG_YELLOW("fds_update() record_id:    0x%04x"DEFAULT_COLOR, config.p_header->record_id);
				//NRF_LOG_YELLOW("fds_update() length_words: %d"DEFAULT_COLOR,     config.p_header->length_words);	
				//NRF_LOG_YELLOW("fds_update() crc16:        0x%04x"DEFAULT_COLOR, config.p_header->crc16);					
				
        /* Copy the configuration from flash into gft4_cfg. */
        memcpy(&gft4_cfg, config.p_data, sizeof(configuration_t));
				
        //NRF_LOG_RED("Device Name: %s, Line:%d"DEFAULT_COLOR, gft4_cfg.device_name, __LINE__);				
				//NRF_LOG_RED("PHY config: %d, %s, Line:%d"DEFAULT_COLOR, gft4_cfg.phy_config.tx_phys, phy_str(gft4_cfg.phy_config), __LINE__);
					
				gft4_cfg.timer_enable       = m_test_params.timer_enable;
				gft4_cfg.erase_bonds        = m_test_params.erase_bonds;
        gft4_cfg.phy_select         = m_test_params.phy_select;
        gft4_cfg.phy_config.tx_phys = m_test_params.phys.tx_phys;
				gft4_cfg.phy_config.rx_phys = m_test_params.phys.rx_phys;
				
			  //(void)sd_ble_gap_phy_update(m_conn_handle, &m_test_params.phys);
			  //NRF_LOG_YELLOW("Config file found, Sending request of updating PHY config to %s"DEFAULT_COLOR, phy_str(m_test_params.phys));

        /* Update boot count. */
        //gft4_cfg.boot_count++;				

        /* Close the record when done reading. */
        rc = fds_record_close(&description);
        APP_ERROR_CHECK(rc);

        /* Write the updated record to flash. */
        rc = fds_record_update(&description, &gft4_config_record);
        //APP_ERROR_CHECK(rc);
				//NRF_LOG_YELLOW("Config file found, updating boot count to %d."DEFAULT_COLOR, gft4_cfg.boot_count);
				if(rc != NRF_SUCCESS) 
				{
					Buzzer_Beep();				
				}
    }
}


/**
 * @}
 */
