/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
/** @file
 * @defgroup vcnl3020.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */

#include <stdio.h>
#include "max14676.h"
#include "vcnl3020.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
//#include "ble_lbs.h"

#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28

static nrfx_err_t vcnl3020_nrfx_twi_txrx(nrfx_twi_t const * p_instance,
                       uint8_t            address,
                       uint8_t *          p_data,
                       size_t             length,
                       uint8_t *          p_data1,
                       size_t             length1)
{
	  ret_code_t err_code;
    nrfx_twi_xfer_desc_t xfer_tx = NRFX_TWI_XFER_DESC_TX(address, p_data, length);
    err_code = nrfx_twi_xfer(p_instance, &xfer_tx, 1);
	  APP_ERROR_CHECK(err_code);

	  nrfx_twi_xfer_desc_t xfer_rx = NRFX_TWI_XFER_DESC_RX(address, p_data1, length1);
	  err_code = nrfx_twi_xfer(p_instance, &xfer_rx, 0);	  
	  APP_ERROR_CHECK(err_code);

	return 0;
}

static void vcnl3020_nrf_drv_twi_txrx(nrf_drv_twi_t const * p_instance,
                            uint8_t               address,
                            uint8_t *             p_data,
                            uint8_t               length,
                            uint8_t *             p_data1,
                            uint8_t               length1)
{
	vcnl3020_nrfx_twi_txrx(&p_instance->u.twi, address, p_data, length, p_data1, length1);
}

void vcnl3020_WriteReg(uint8_t regAddr,uint8_t regValue) 
{
	ret_code_t err_code;
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
	
  err_code = nrf_drv_twi_tx(&m_twi, VCNL3020_DEV_ADDR, send_Buf, 2, false);			
  APP_ERROR_CHECK(err_code);
}

static void vcnl3020_Write16(uint8_t regAddr, uint16_t TH_Value) 
{
	ret_code_t err_code;
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)((TH_Value >> 8)&0xff);
	send_Buf[2] = (uint8_t)((TH_Value >> 0)&0xff);
	
  err_code = nrf_drv_twi_tx(&m_twi, VCNL3020_DEV_ADDR, send_Buf, 3, false);			
  APP_ERROR_CHECK(err_code);
}

uint32_t vcnl3020_ReadReg(uint8_t regAddr, uint8_t* ptrVal) 
{
	ret_code_t err_code;
  uint8_t readValue[2];
  
	err_code = nrf_drv_twi_tx(&m_twi, VCNL3020_DEV_ADDR, &regAddr, 1, true);
	if(err_code != NRF_SUCCESS)
		return err_code;

  err_code = nrf_drv_twi_rx(&m_twi, VCNL3020_DEV_ADDR, readValue, 2);
	if(err_code != NRF_SUCCESS)
		return err_code;
			
  *ptrVal = readValue[0];
  return  err_code; 
}

uint8_t vcnl3020_Read(uint8_t regAddr) 
{
  ret_code_t err_code;
  uint8_t readValue[2];
   
  err_code = nrf_drv_twi_tx(&m_twi, VCNL3020_DEV_ADDR, &regAddr, 1, true);
  APP_ERROR_CHECK(err_code);
	
  err_code = nrf_drv_twi_rx(&m_twi, VCNL3020_DEV_ADDR, readValue, 2);
  APP_ERROR_CHECK(err_code);
	
  return  readValue[0]; 
}

uint16_t vcnl3020_Read16(uint8_t regAddr) 
{
  ret_code_t err_code;
  uint8_t readValue[2];
   
  err_code = nrf_drv_twi_tx(&m_twi, VCNL3020_DEV_ADDR, &regAddr, 1, true);
  APP_ERROR_CHECK(err_code);
	
  err_code = nrf_drv_twi_rx(&m_twi, VCNL3020_DEV_ADDR, readValue, 2);
  APP_ERROR_CHECK(err_code);
	
  return  (uint16_t)(readValue[0] * 256 + readValue[1]); 
}

uint8_t vcnl3020_Read01(uint8_t regAddr) 
{
  uint8_t readValue[2];

  vcnl3020_nrf_drv_twi_txrx(&m_twi, VCNL3020_DEV_ADDR, &regAddr, 1, readValue, 2);		
	
  return  readValue[0]; 
}

void vcnl3020_GetVer(void) 
{
  uint8_t readValue;    
  (void)vcnl3020_ReadReg(VCNL3020_ID_REVISION_REG1, &readValue);  
  if(readValue == 0x21)
    ;
  else 
  {
     ;
  }
}

#define THRESH_HIGH_BYTE                    0x09
#define THRESH_LOW_BYTE                     0x7F
static bool prox_int_flag = false;
static void Clear_Interrupt(void)
{
	vcnl3020_WriteReg(VCNL3020_COMMAND_REG0,      0x00);	    
	vcnl3020_WriteReg(VCNL3020_HIGH_THRESH_REG12, THRESH_HIGH_BYTE);
  vcnl3020_WriteReg(VCNL3020_HIGH_THRESH_REG13, THRESH_LOW_BYTE);			
  nrf_delay_ms(100);
  vcnl3020_WriteReg(VCNL3020_COMMAND_REG0,      PROX_EN|SELFTIMED_EN);   // 0x03
	nrf_delay_ms(100);
}

void prox_int_event_handler(uint8_t pin_no, uint8_t int_action)
{	
    switch (pin_no)
    {
        case PROX_INT:
				    if(int_action)
						{
							  prox_int_flag = true;
								//BRIGHT_LED_ON();							  
						}
						else
						{
							  //BRIGHT_LED_OFF();
						}
            break;				
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

void prox_init(void) 
{
	nrf_delay_ms(10);
	vcnl3020_GetVer();

	vcnl3020_WriteReg(VCNL3020_PROX_MOD_TIMING_ADJ_REG15, 0x00);
	vcnl3020_WriteReg(VCNL3020_PROX_RATE_REG2,    0x00);
	vcnl3020_WriteReg(VCNL3020_IRRED_CUR_REG3,    0x00);
	vcnl3020_WriteReg(VCNL3020_INT_CTRL_REG9,     0x00);
	vcnl3020_Write16 (VCNL3020_LOW_THRESH_REG10,  0x00);
	vcnl3020_Write16 (VCNL3020_HIGH_THRESH_REG12, 0x00);
	vcnl3020_WriteReg(VCNL3020_COMMAND_REG0,      0x00);
	nrf_delay_ms(100);
  vcnl3020_WriteReg(VCNL3020_PROX_RATE_REG2,             PROX_RATE_250_000);
	vcnl3020_WriteReg(VCNL3020_IRRED_CUR_REG3,             10);	              
	vcnl3020_WriteReg(VCNL3020_PROX_MOD_TIMING_ADJ_REG15,0x01); 	
	
	vcnl3020_WriteReg(VCNL3020_HIGH_THRESH_REG12,        THRESH_HIGH_BYTE);                //16);/
  vcnl3020_WriteReg(VCNL3020_HIGH_THRESH_REG13,        THRESH_LOW_BYTE);                 //E4);
  vcnl3020_WriteReg(VCNL3020_INT_CTRL_REG9  ,          INT_COUNT_EXCEED_64|INT_THRES_EN);//0x62);

	vcnl3020_Read(VCNL3020_PROX_RESULT_REG7);
	vcnl3020_Read(VCNL3020_PROX_RESULT_REG8);
	vcnl3020_WriteReg(VCNL3020_INT_STATUS_REG14,         0x01);	
  nrf_delay_ms(100);
  vcnl3020_WriteReg(VCNL3020_COMMAND_REG0,             PROX_EN|SELFTIMED_EN);              // Start measurement
	nrf_delay_ms(100);
}

void prox_process(void)
{
	uint8_t int_status;
	if(prox_int_flag)
	{
		prox_int_flag = false;			
		int_status= vcnl3020_Read(VCNL3020_INT_STATUS_REG14);
		if(int_status & INT_TH_HI)
		{
			Clear_Interrupt();
		}
	}
}


/** @} */



