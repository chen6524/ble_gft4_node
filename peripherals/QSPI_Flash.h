/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _QSPI_FLASH_H_
#define _QSPI_FLASH_H_

#include <stdint.h>

#define QSPI_FLASH_PageSize       256

#define QSPI_FLASH_SectorNum      0x800
#define QSPI_FLASH_SectorSize     0x1000       // 4K
#define QSPI_FLASH_BlockNum       0x80
#define QSPI_FLASH_BlockSize      0x10000      // 64K
#define QSPI_FLASH_SIZE           0x800000     // 8912K

void QSPI_Flash_Init(void);
void QPSI_FLASH_SectorErase4K(uint32_t SectorAddr);
void QPSI_FLASH_SectorErase64K(uint32_t SectorAddr);
void QSPI_FLASH_PageWrite(uint8_t* WriteAddr, uint32_t Write_Addr, uint16_t NumByteToWrite);
void QSPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
#endif

