/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#include <stdio.h>
#include "max14676.h"
#include "timer.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_timer.h"
#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28
#include "Low_PWM_Leds.h"

/** @file
 * @defgroup timer.c
 +
 *
 * This file contains the source code for a sample application using timer
 *
 */
 #define TIMER_INTERVAL_MS                                        1     //Time(in miliseconds) between consecutive compare events.
 // Timer 0 for SoftDevice, can't be used for application
 // Timer 1 for Buzzer
 // Timer 2, Timer 3, Timer 4 can be used
 const nrf_drv_timer_t TIMER_GFT4 = NRF_DRV_TIMER_INSTANCE(4);
 
 /**
 * @brief Handler for timer events.
 */
uint32_t timer_1_ms = 0;
void timer_led_event_handler(nrf_timer_event_t event_type, void* p_context)
{
	  static uint32_t timer_cnt = 0;
//	  static bool led_flag;
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:					 
				  { 
						timer_1_ms++;
					  if(timer_cnt == 0)
					  {
              //BRIGHT_LED_ON();                                  // second rate
              timer_cnt++;			
		        }
						else if((timer_cnt > 0)&&(timer_cnt < 10))
						{
							timer_cnt++;
						}
						else if(timer_cnt == 10)
						{
							//BRIGHT_LED_OFF();
							timer_cnt++;
						}
		        else if((timer_cnt >= 10)&&(timer_cnt < 1000))
		        {
		           //BRIGHT_LED_OFF();
			         timer_cnt++;
		        }
		        else
		        {
			        timer_cnt = 0;
		        }
				  }				
#if 0
          timer_cnt++;
					if(timer_cnt > 10)
					{						
						timer_cnt = 0;
					  led_flag =!led_flag;
					  if(led_flag)
						  BRIGHT_LED_ON();
					  else
						  BRIGHT_LED_OFF();
				  }
					
#endif					
          break;
					
        default:
            //Do nothing.
            break;
    }
}

void accurate_timer_init(void)
{
	uint32_t err_code = NRF_SUCCESS;
	uint32_t time_ticks;
	
  //Configure TIMER_LED for generating simple light effect - leds on board will invert his state one after the other.
  nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
  err_code = nrf_drv_timer_init(&TIMER_GFT4, &timer_cfg, timer_led_event_handler);
  APP_ERROR_CHECK(err_code);

  time_ticks = nrf_drv_timer_ms_to_ticks(&TIMER_GFT4, TIMER_INTERVAL_MS);

  nrf_drv_timer_extended_compare(&TIMER_GFT4, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

  nrf_drv_timer_enable(&TIMER_GFT4);
}
 
 /** @} */


