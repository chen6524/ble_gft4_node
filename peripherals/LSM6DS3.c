/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
/** @file
 * @defgroup LSM6DS3.c
 +
 *
 * This file contains the source code for a sample application using SPI
 *
 */

#include <stdio.h>
#include "max14676.h"
#include "LSM6DS3.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_spi.h"
//#include "ble_lbs.h"

#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define SPI_SCK_PIN   42
#define SPI_MISO_PIN  45
#define SPI_MOSI_PIN  47
#define SPI_SS_PIN    41

#define SPI_INSTANCE  2                                               /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;                                   /**< Flag used to indicate that SPI instance completed the transfer. */

static uint8_t       m_tx_buf[16] = {Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,
                                     Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte,Dummy_Byte};                          /**< TX buffer. */
static uint8_t       m_rx_buf[16];                          /**< RX buffer. */
#define MAX_LENGTH   16                                     /**< Max Transfer length. */

#if  0
/**
 * @brief SPI user event handler.
 * @param event
 */
static void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context)
{
    spi_xfer_done = true;
    NRF_LOG_INFO("Transfer completed.");
    if (m_rx_buf[0] != 0)
    {
        NRF_LOG_INFO(" Received:");
        NRF_LOG_HEXDUMP_INFO(m_rx_buf, strlen((const char *)m_rx_buf));
    }
}
#endif
																		 
void acc_int_event_handler(uint8_t pin_no, uint8_t int_action)
{
//    ret_code_t err_code;
//	  uint8_t send_buf[5];

    switch (pin_no)
    {
        case ACC_INT1_AG:
				    if(int_action)
						{
							//BRIGHT_LED_ON();
						}
						else
						{
							//BRIGHT_LED_OFF();
						}
            break;
				case ACC_INT2_DEN_AG:
            break;					
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

void LSM6DS3_Init(void)
{
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = SPI_SS_PIN;
    spi_config.miso_pin = SPI_MISO_PIN;
    spi_config.mosi_pin = SPI_MOSI_PIN;
    spi_config.sck_pin  = SPI_SCK_PIN;
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, /*spi_event_handler*/NULL, NULL));	
	  nrf_delay_ms(5); 
    SPI_MEMS_Acc_Config(0);
    SPI_MEMS_Gyro_Config();		  
}


//static char FIFO_Src       = 0;
//static char FIFO_Ovr       = 0;

/*******************************************************************************
* Function Name  : Gyroscope SPI_BYTE/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/

#if 0
/*******************************************************************************
* Function Name  : FifoBypassToStream
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_BypassToStream(void) 
{
  uint8_t data;
  
  // Clear Interrupt if LIR enabled
  //do {
    data = gyro_SPIReadReg(GYRO_INT1_SRC);    
  //} while( (data & INT_SRC_IA) );
  
  // Interrupt spend 1/2 samples to go low!!!
			
  data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  data = data | CTRL_REG5_FIFO_EN;
  gyro_SPIWriteReg(GYRO_CTRL_REG5,data);
  
  data = gyro_SPIReadReg(GYRO_FIFO_CTRL_REG);
  data = (data & FIFO_CONTROL_WTMSAMP) | DEF_FIFO_CTRL_BYPASS_TO_STREAM;
  gyro_SPIWriteReg(GYRO_FIFO_CTRL_REG,data);  
}

#endif

 /*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/
// Real Data start from second byte ( pBuffer[1] )
void acc_SPIBufferRead(uint8_t* pBuffer, uint8_t readAddr, uint8_t numByteToRead)
{ 
	m_tx_buf[0] = readAddr |0x80;
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, m_tx_buf, numByteToRead + 1, pBuffer, numByteToRead + 1));		
}

void acc_FIFO_SPIGetRawData(uint8_t *pBuffer) 
{ 
  acc_SPIBufferRead(pBuffer, LSM6DS3_ACCEL_OUT_X_L_ADDR, 6); 
  
  pBuffer[0] = pBuffer[1 + 1];
  pBuffer[1] = pBuffer[0 + 1];
  
  pBuffer[2] = pBuffer[3 + 1];
  pBuffer[3] = pBuffer[2 + 1];
  
  pBuffer[4] = pBuffer[5 + 1];
  pBuffer[5] = pBuffer[4 + 1];
}

#if 0
void gyro_FIFO_SPIGetRawData(uint8_t *pBuffer)
{  
	  acc_SPIBufferRead(pBuffer, LSM6DS3_GYRO_OUT_X_L_ADDR, 6); 
	  
	  pBuffer[0] = pBuffer[1];
	  pBuffer[1] = pBuffer[0];
	  
	  pBuffer[2] = pBuffer[3];
	  pBuffer[3] = pBuffer[2];
	  
	  pBuffer[4] = pBuffer[5];
	  pBuffer[5] = pBuffer[4];
}
#else
void gyro_FIFO_SPIGetRawData(uint8_t *pBuffer)
{
	acc_SPIBufferRead(pBuffer, LSM6DS3_FIFO_DATA_OUT_L, 6);
			
  pBuffer[0] = pBuffer[1 + 1];
  pBuffer[1] = pBuffer[0 + 1];
  
  pBuffer[2] = pBuffer[3 + 1];
  pBuffer[3] = pBuffer[2 + 1];
  
  pBuffer[4] = pBuffer[5 + 1];
  pBuffer[5] = pBuffer[4 + 1];
}
#endif

uint8_t acc_SPIReadReg(uint8_t regAddr) 
{
  uint8_t regAddr2[2];
	uint8_t readValue[2];
  
	regAddr2[0] = regAddr |0x80;
	regAddr2[1] = Dummy_Byte;	
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, regAddr2, 2, readValue, 2));			
  
  return readValue[1];   
}

/*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Write
* Description    : write a Byte to SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  WriteAddr is the register address you want to write to
*                  pBuffer contains bytes to write
* Output         : None
* Return         : None
*******************************************************************************/
void acc_SPIWriteReg(uint8_t regAddr, uint8_t regValue)
{    
	 uint8_t regAddr2[2];
   uint8_t readValue[2];

	regAddr2[0] = regAddr;
	regAddr2[1] = regValue;			
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, regAddr2, 2, readValue, 2));			
 }

void acc_SPIBufferWrite(uint8_t regAddr, uint8_t *writeBuf, uint8_t numByteToWrite) 
{
  uint8_t i;
  
  if(numByteToWrite > 1)
    regAddr |= 0x40;
  
	m_tx_buf[0] = regAddr;
	for(i = 0;i<numByteToWrite;i++) 
	{
		m_tx_buf[i+1] = writeBuf[i];
	}
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, &m_tx_buf[0], numByteToWrite + 1, m_rx_buf, numByteToWrite + 1));	
	
}

/*******************************************************************************
* Function Name  : SPI2_MEMS_Init
* Description    : I2C Initialization
* Input          : None 
* Output         : None
* Return         : None
*******************************************************************************/
#define INT1_THRESHOLD_VALUE   12                                   //  125 * 12/1000 = 1.5g
void SPI_MEMS_Acc_Config(uint8_t mode)
{ 
  volatile uint8_t abc=0; 

 //if(mode == 0) 
   {
   #if 0
   //acc_SPIWriteReg(ACC_CTRL_REG5,FIFO_STREAM_A);          // Reboot this acc each time restart, 2014.05.07
     nrf_delay_ms(100);                                     // wait a while make it working.
     
     abc =  acc_SPIReadReg(0x0F);
     // CTRL_REG1 
     acc_SPIWriteReg(LSM6DS3_ACCEL_AXIS_EN_ADDR,  0x38);    // enable acc x,y,z  
      
   //acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x08);    // 416hz,4g range   
   //acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x0C);    // 416hz,8g range
     acc_SPIWriteReg(LSM6DS3_ACCEL_ODR_ADDR,0x60| 0x04);    // 416hz,16g range
   
   //interrupt pin assignment 
   
     acc_SPIWriteReg(LSM6DS3_MD1_ADDR,        0x04);    
     acc_SPIWriteReg(LSM6DS3_WAKE_UP_THS_ADDR,0x84|0x06);
   #else
     // Added by Jason Chen
                                                                // double-tap event, 6D even and tilt event 
     acc_SPIWriteReg(LSM6DS3_CTRL3_C_ADDR,    0xB1);            // Reboot this device and reset memmery each time restart, Recommended by Jason Chen, 2016.01.13,LSM6DS3_BDU_ADDR
		                                                            // Interrupt activation level: Low
																																// Interrupt pin open drain mode		 
     nrf_delay_ms(100);                                         // wait a while make it working.
     
     abc =  acc_SPIReadReg(0x0F);
  // CTRL_REG1 
     acc_SPIWriteReg(LSM6DS3_CTRL9_XL_ADDR,   0x38);            // enable acc x,y,z  
          
     // Wake-up and 6D Interrupt configr=uration                                                           
     acc_SPIWriteReg(LSM6DS3_CTRL1_XL_ADDR,   0x64);            // 416hz,16g range                                    
     acc_SPIWriteReg(LSM6DS3_CTRL8_XL_ADDR,   0x01);            // Low-pass filter on 6D function selection for 6D event recognition                                    
     acc_SPIWriteReg(LSM6DS3_TAP_CFG_ADDR,    0x10);            // Enable accelerometer HP and LPF2 filters for wakeup event, no interrupt latched    
     acc_SPIWriteReg(LSM6DS3_WAKE_UP_DUR_ADDR,0x00);            // No duration
     acc_SPIWriteReg(LSM6DS3_WAKE_UP_THS_ADDR,0x02);            // Disable Single-tap and Double-tap, and Threshold for wake up --> 2 = 2*ODR_XL time                                                              
     acc_SPIWriteReg(LSM6DS3_TAP_THS_6D,      0x61);            // 6D Threshold Value = 50 degree, Tap threshed 1 ---> ? value
     acc_SPIWriteReg(LSM6DS3_MD1_CFG_ADDR,    0x24);            // Rounting on INT1 of wakeup event, 6D event
   #endif 
   }
}

void SPI_MEMS_Gyro_Config(void)
{  
   #if 1
   //volatile uint8_t mBuffer[6];
   //volatile uint8_t a;
// acc_SPIWriteReg(LSM6DS3_GYRO_ODR_ADDR,  0x6C);    // 416hz 2000dps
   acc_SPIWriteReg(LSM6DS3_GYRO_ODR_ADDR,  0x7C);    // 833hz 2000dps
   //fifo setup
   acc_SPIWriteReg(LSM6DS3_FIFO_THR_L_ADDR,0x60);
   //acc_SPIWriteReg(LSM6DS3_FIFO_THR_H_ADDR,0x0);
   acc_SPIWriteReg(LSM6DS3_INT2_ON_INT1_ADDR,0x5);   //Stop_on_FTH , and I2C disabled
   acc_SPIWriteReg(LSM6DS3_FIFO_CTRL3_ADDR,0x08);    //decimation: 0 , and Accelerometer sensor not in FIFO
   acc_SPIWriteReg(LSM6DS3_FIFO_MODE_ADDR,0x3e);     //continue ;833hz
   #endif
   acc_SPIWriteReg(LSM6DS3_FUNC_EN_ADDR,0x38);       // Gyro x,y,z output enabled

   
   //Cpu_Delay100US(1000); 
   //a = acc_SPIReadReg(0x3a ); 
  // a = acc_SPIReadReg(0x3b ); 

   
   //gyroGet();
   //gyro_FIFO_SPIGetRawData(mBuffer);
   //a= mBuffer[0];

}


/** @} */



