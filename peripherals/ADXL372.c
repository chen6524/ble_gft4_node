/***************************************************************************
 *
 *            Copyright (c) 2019 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 174 W Beaver Creek Rd.
 * Richmond Hill, ON, L4B 1B4
 * Canada
 *
 * Tel:   (905) 479-0109
 * Fax:   (905) 479-0621
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
/** @file
 * @defgroup ADXL372.c
 *
 * This file contains the source code for a sample application using SPI
 *
 */

#include <stdio.h>
#include <string.h>
#include "max14676.h"
#include "ADXL372.h"
//#include "LSM6DS3.h"
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_spi.h"
//#include "ble_lbs.h"

#include "nrf_delay.h"                                   // Added by Jason Chen, 2019.10.28
#include "Low_PWM_Leds.h"


#define NRFX_SPIM_SCK_PIN  38
#define NRFX_SPIM_MOSI_PIN 34
#define NRFX_SPIM_MISO_PIN 39
#define NRFX_SPIM_SS_PIN   36
#define NRFX_SPIM_DCX_PIN  32             // P1.0, not used 


#define SPI_INSTANCE  3                                           /**< SPI instance index. */
static const nrfx_spim_t spi = NRFX_SPIM_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done;                                   /**< Flag used to indicate that SPI instance completed the transfer. */

static uint8_t       m_tx_buf[16] = {ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,
                                     ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE,ADXL_DUMMY_BYTE};   /**< TX buffer. */
static uint8_t       m_rx_buf[512];                         /**< RX buffer. */
static uint8_t       fifo_buf[512];                         /**< FIFO buffer. */
static int16_t       peak_data[6];                          /**< FIFO buffer. */
																		 
static bool hg_int1_flag = false;
																		 
void int_hg_event_handler(uint8_t pin_no, uint8_t int_action)
{
    switch (pin_no)
    {
        case INT1_HG:
				    if(int_action)
						{
							//BRIGHT_LED_ON();
							hg_int1_flag = true;
						}
						else
						{
							//BRIGHT_LED_OFF();
						}
						BRIGHT_LED_ON();
            break;
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}

 /*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/
void ADXL372_SPIBufferRead(uint8_t* pBuffer, uint8_t readAddr, uint8_t numByteToRead)
{ 
	m_tx_buf[0]  = (readAddr << 1) & 0xFE;
	m_tx_buf[0] |= 0x01;
	nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, numByteToRead + 1, m_rx_buf, numByteToRead + 1);		
	APP_ERROR_CHECK(nrfx_spim_xfer(&spi, &xfer_desc, 0));
	
	memcpy(pBuffer, &m_rx_buf[1], numByteToRead);
}

uint8_t ADXL372_SPIReadReg(uint8_t regAddr) 
{
  uint8_t regAddr2[2];
	uint8_t readValue[2];
  	
	regAddr2[0]  = (regAddr << 1) & 0xFE;
	regAddr2[0] |= 0x01;
	regAddr2[1]  = ADXL_DUMMY_BYTE;
	nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(regAddr2, 2, readValue, 2);		
	APP_ERROR_CHECK(nrfx_spim_xfer(&spi, &xfer_desc, 0));  
  return readValue[1];   
}

/*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Write
* Description    : write a Byte to SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  WriteAddr is the register address you want to write to
*                  pBuffer contains bytes to write
* Output         : None
* Return         : None
*******************************************************************************/
void ADXL372_SPIWriteReg(uint8_t regAddr, uint8_t regValue)
{    
	 uint8_t regAddr2[2];
   uint8_t readValue[2];

	regAddr2[0] = (regAddr << 1) & 0xFE;
	regAddr2[1] = regValue;				
	nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(regAddr2, 2, readValue, 2);		
	APP_ERROR_CHECK(nrfx_spim_xfer(&spi, &xfer_desc, 0)); 
 }

void ADXL372_SPIBufferWrite(uint8_t regAddr, uint8_t *writeBuf, uint8_t numByteToWrite) 
{
  uint8_t i;
  
  //if(numByteToWrite > 1)
  //  regAddr |= 0x40;
  
	m_tx_buf[0] = (regAddr << 1) & 0xFE;
	for(i = 0;i<numByteToWrite;i++) 
	{
		m_tx_buf[i+1] = writeBuf[i];
	}
	nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, numByteToWrite + 1, m_rx_buf, numByteToWrite + 1);
	APP_ERROR_CHECK(nrfx_spim_xfer(&spi, &xfer_desc, 0));
}

void ADXL372_Reset(void)
{
	ADXL372_SPIWriteReg(ADXL372_RESET, 0x52);
}

static bool ADXL372_Test(void)
{
	uint8_t retValue, testCount;
	uint8_t rxBuffer[4];
	
	ADXL372_Reset();
	nrf_delay_ms(500);
	
	ADXL372_SPIBufferRead(rxBuffer, ADXL372_DEVID_AD, 4);        // Test Burst Read ( multiple byte read)
	
	if(rxBuffer[0] != DEVICE_ID) 
	{
		BRIGHT_LED_ON();	
		while(true);
	}
	else if(rxBuffer[1] != DEVID_MST)
	{
		BRIGHT_LED_ON();	
		while(true);
	}
	else if(rxBuffer[2] != DEVID_PRODUCT)
	{
		BRIGHT_LED_ON();	
		while(true);
	}
	else if(rxBuffer[3] != REVID)
	{
		BRIGHT_LED_ON();	
		while(true);
	}

	ADXL372_SPIWriteReg(ADXL372_POWER_CTL, MEASUREMENT_MODE);     // Mode- measurement mode

	ADXL372_SPIWriteReg(ADXL372_SELF_TEST, 0x01);                 // Start Testing
	nrf_delay_ms(350);
	retValue = ADXL372_SPIReadReg(ADXL372_SELF_TEST);
	testCount = 0;
	while(true)
	{ 
		testCount++;
		if( retValue&0x02)
		{
			return (retValue & 0x04);                                // Test Finished, return its result
		}
		if(testCount < 100)
		{
		  nrf_delay_ms(10);
		  retValue = ADXL372_SPIReadReg(ADXL372_SELF_TEST);
		}
		else
		{
			ADXL372_SPIWriteReg(ADXL372_SELF_TEST, 0x00);            // Stop Test
			return false;
		}
	}
}

static void ADXL372_Initialization(void)
{
	uint16_t act_threshold;
	uint8_t  act_threshold_h, act_threshold_l;
	
	ADXL372_SPIWriteReg(ADXL372_POWER_CTL,        FILTER_SETTLE_370MS|STANDBY_MODE);     // make it Standby mode for changes to activity or inactivity
			
	act_threshold   = 10 * 10;                                                           // Activity Threshold = 60g, 10LSB/g
	act_threshold   = act_threshold&0x7FF;
	act_threshold   = act_threshold << 5;
	act_threshold_h = (act_threshold >> 8) & 0xFF;
	act_threshold_l = (act_threshold >> 0) & 0xE0;
	act_threshold_l = act_threshold_l | 0x1;                                             // Enable the axis, No REF
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_X_H,   act_threshold_h); 
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_X_L,   act_threshold_l);		
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_Y_H,   act_threshold_h);
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_Y_L,   act_threshold_l);
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_Z_H,   act_threshold_h);
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT_Z_L,   act_threshold_l);
	act_threshold_l = 30;  // = (200/6.6)                                                // 200ms, 6.6ms/LSB for ODR=3200Hz
	ADXL372_SPIWriteReg(ADXL372_TIME_ACT,         act_threshold_l);
	
	act_threshold   = 2 * 10;                                                            // Inactivity Threshold = 2g, 10LSB/g
	act_threshold   = act_threshold&0x7FF;
	act_threshold   = act_threshold << 5;
	act_threshold_h = (act_threshold >> 8) & 0xFF;
	act_threshold_l = (act_threshold >> 0) & 0xE0;
	act_threshold_l = act_threshold_l | 0x1;                                             // Enable the axis
	ADXL372_SPIWriteReg(ADXL372_THRESH_INACT_X_H, act_threshold_h);
	ADXL372_SPIWriteReg(ADXL372_THRESH_INACT_X_L, act_threshold_l);
	ADXL372_SPIWriteReg(ADXL372_THRESH_INACT_Y_H, act_threshold_h);
	ADXL372_SPIWriteReg(ADXL372_THRESH_INACT_Y_L, act_threshold_l);
	ADXL372_SPIWriteReg(ADXL372_THRESH_INACT_Z_H, act_threshold_h);
	ADXL372_SPIWriteReg(ADXL372_THRESH_INACT_Z_L, act_threshold_l);
  act_threshold = 2000/26;                                                              // Inactivity time count, 2000ms, 26ms/LSB for ODR = 32000Hz	
	act_threshold_h = (act_threshold >> 8) & 0xFF;
	act_threshold_l = (act_threshold >> 0) & 0xFF;
	ADXL372_SPIWriteReg(ADXL372_TIME_INACT_H,     act_threshold_h);
	ADXL372_SPIWriteReg(ADXL372_TIME_INACT_L,     act_threshold_l);	
	
	act_threshold   = 5 * 10;                                                             // Activity Threshold = 30g, 10LSB/g
	act_threshold   = act_threshold&0x7FF;
	act_threshold   = act_threshold << 5;
	act_threshold_h = (act_threshold >> 8) & 0xFF;
	act_threshold_l = (act_threshold >> 0) & 0xE0;
	act_threshold_l = act_threshold_l | 0x1;                                               // Enable the axis, No REF	
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT2_X_H,  act_threshold_h);
  ADXL372_SPIWriteReg(ADXL372_THRESH_ACT2_X_L,  act_threshold_l);
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT2_Y_H,  act_threshold_h);
  ADXL372_SPIWriteReg(ADXL372_THRESH_ACT2_Y_L,  act_threshold_l);
	ADXL372_SPIWriteReg(ADXL372_THRESH_ACT2_Z_H,  act_threshold_h);
  ADXL372_SPIWriteReg(ADXL372_THRESH_ACT2_Z_L,  act_threshold_l);
	
  ADXL372_SPIWriteReg(ADXL372_HPF,              HPF_01_98HZ_BY_3200HZ);	                 // High Pass Filter Corner = 1.98Hz for ODR = 3200Hz
  ADXL372_SPIWriteReg(ADXL372_FIFO_SAMPLES,     32);                                     // Store 32 samples after one activity event trigger, store (170 - 32) before the trigger	
	ADXL372_SPIWriteReg(ADXL372_FIFO_CTL,         FIFO_XYZ_PEAK_ALL|FIFO_TRIGGER_MODE);    // FIFO_TRIGGER_MODE ===> 0x3C = 0b0011110x   
	                                                                                       // FIFO_STREAM_MODE  ===> 0x3A = 0b0011101x	
																																												 
	ADXL372_SPIWriteReg(ADXL372_INT1_MAP,         INT1_LOW_ACTIVE|ACTIVE_INT1|INACTIVE_INT1|FIFO_FULL_INT1);	
	ADXL372_SPIWriteReg(ADXL372_TIMING,           ODR_3200 | WAKEUP_RATE_104MS);
	
	ADXL372_SPIWriteReg(ADXL372_MEASURE,          LINKLOOP_LOOP_MODE|BANDWIDTH_1600HZ);	   // Looped Mode, 1600Hz Bandwidth, Normal operation ( No loww Noise)

//ADXL372_SPIWriteReg(ADXL372_POWER_CTL,        FILTER_SETTLE_370MS|LPF_ENABLE|HPF_ENABLE|MEASUREMENT_MODE);
	ADXL372_SPIWriteReg(ADXL372_POWER_CTL,        FILTER_SETTLE_370MS|MEASUREMENT_MODE);
}

void ADXL372_Init(void)
{
	
    //nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, MAX_LENGTH, m_rx_buf, MAX_LENGTH);

    nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG;
    spi_config.frequency      = NRF_SPIM_FREQ_4M;
    spi_config.ss_pin         = NRFX_SPIM_SS_PIN;
    spi_config.miso_pin       = NRFX_SPIM_MISO_PIN;
    spi_config.mosi_pin       = NRFX_SPIM_MOSI_PIN;
    spi_config.sck_pin        = NRFX_SPIM_SCK_PIN;
  //spi_config.dcx_pin        = NRFX_SPIM_DCX_PIN;
  //spi_config.use_hw_ss      = true;
    spi_config.ss_active_high = false;
    APP_ERROR_CHECK(nrfx_spim_init(&spi, &spi_config, NULL, NULL));

	  nrf_delay_ms(100); 
	
	  if(!ADXL372_Test())
		{
	    White_Led(LED_ON);while(true);
		}
		
		ADXL372_Initialization();		
}

uint8_t StatusRegister(void)
{
	return ADXL372_SPIReadReg(ADXL372_STATUS);
}

uint8_t ActivityStatusRegister(void)
{
	return ADXL372_SPIReadReg(ADXL372_STATUS2);
}

void ADXL372_MAXPEAK_DataRead(int16_t *pBuffer) 
{ 
  int16_t data_value;
	uint8_t mBuffer[6];
	
	ADXL372_SPIBufferRead(mBuffer, ADXL372_MAXPEAK_X_H, 6);                  // read Max Peak impact
	
	data_value = mBuffer[0];
	data_value = (data_value << 8) & 0xFF00;
	data_value =  data_value |  mBuffer[1];
	pBuffer[0] = data_value/16;

	data_value = mBuffer[2];
	data_value = (data_value << 8) & 0xFF00;
	data_value =  data_value |  mBuffer[3];
	pBuffer[1] = data_value/16;

	data_value = mBuffer[4];
	data_value = (data_value << 8) & 0xFF00;
	data_value =  data_value |  mBuffer[5];
	pBuffer[2] = data_value/16;	
}

void ADXL372_FIFO_DataRead(uint8_t *pBuffer, uint8_t data_len) 
{ 	
	ADXL372_SPIBufferRead(pBuffer, ADXL372_FIFO_DATA, data_len * 3);        // read Max Peak impact
}

void hg_process(void)
{
	uint8_t status, status2;
	
	if(hg_int1_flag)
	{
		hg_int1_flag = false;
		
		status  = ADXL372_SPIReadReg(ADXL372_STATUS);
		status2 = ADXL372_SPIReadReg(ADXL372_STATUS2);
				
		ADXL372_MAXPEAK_DataRead(peak_data); 
		ADXL372_FIFO_DataRead(fifo_buf, 169);
		BRIGHT_LED_OFF();
		
	}
}

/** @} */



